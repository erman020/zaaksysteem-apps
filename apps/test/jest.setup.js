const fetch = require('./double/stub/fetch');
const assign = require('./double/fake/location');

window.fetch = fetch;
window.location.assign = assign;
