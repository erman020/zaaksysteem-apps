const { WEBPACK_BUILD_TARGET } = process.env;

const defaultPlugins = ['@babel/plugin-proposal-class-properties'];

const productionPlugins = [
  [
    'react-remove-properties',
    {
      properties: ['scope', 'data-scope'],
    },
  ],
];

const config = {
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: [
    ...defaultPlugins,
    ...(WEBPACK_BUILD_TARGET === 'production' ? productionPlugins : []),
  ],
};

module.exports = function(api) {
  api.cache(true);
  return config;
};
