# Documentation

The [ESDoc](https://esdoc.org/) documentation that you are reading
right now can be generated and viewed on the host system by running

    🐳 zs rtfm

in the container.

## Configuration

The main configuration file is located at `/apps/.esdoc`.
External documentation links are defined in `/apps/esdoc.js`.

## Manual

The manual is a collection of Markdown files with a high level description
of the tech stack and architecture of this respository.

Its table of contents is maintained in the `manual` plugin section
of the [configuration](#configuration) file.

## DocBlock

*DocBlock* comments are annotations in the source code above an identifier.
Their generic form is:

    /**
     * <description>
     * <tags>
     */
     <identifier>
     
### Tags

* [Complete ESDoc tags documentation](https://esdoc.org/manual/tags.html)

The most common tags are `@param` and `@return`:

    @param {<type>} <identifier> <description>
    @return {<type>} <description>

You can put the description indented on a new line to improve readability:

    @return {<type>}
      <description>

Parameters can be marked as optional by enclosing them in square brackets,
and optional parameters can have a default value:

    @param {<type>} [<identifier>] <description>
    @param {<type>} [<identifier>=42] <description>

Providing a usage example is helpful if the parameters or 
return value are complex, or if the context syntax is unclear.

    @example
    <example code>

Always annotate tests with the identifier of the object under 
test for cross reference hyperlinks in the API documentation.

    @test {<identifier>}

You can explicitly exclude exported identifiers from the documentation.
Exporting internal identifiers is useful for testing, and their API
documentation is still useful during development for IDEs that process them.

    @ignore

### Types

Object instances are indicated with their constructor, e.g.

    {Array}
    {Function}
    {Object}
    
NB: Unlike with primitives, it doesn't matter if an array or object
was actually created with the initializer or the constructor, or if
a function was created with a declaration or an expression, or 
constructed.

You can also indicate *Host Objects*, e.g.

    {Node}
    {Element}
    {HTMLElement}
    {HTMLHtmlElement}

Be as thorough as you reasonably can when inheritance is concerned. 
In the above examples, `HTMLHtmlElement` (i.e. `document.documentRoot`)
inherits from `HTMLElement`, which inherits from `Element`, which 
inherits from `Node`. If you wrote the code yourself, you should
**know** if the expected parameter is the actual `HTML` element and 
should choose `{HTMLHtmlElement}` in that case. If you document existing 
code later on, you might not even know if a text node could be passed
(e.g. in a loop) and should choose `{Node}`.

Lastly you can indicate user-defined objects, 
be it from from library code or your own, e.g.

    {ReactElement}

Host objects and user-defined objects need external documentation 
declarations, see [Configuration](#configuration).

Primitives are always lowercase:

    {boolean}
    {number}
    {null}
    {string}
    {undefined}

Beware that related constructors do not return primitives but objects, 
in that case the technically correct annotations would be

    {Boolean}
    {Number}
    {String}

Please avoid that in the first place, it has no advantages 
and is a common source of `typeof` related bugs.

If the type of a value is overloaded by design, or you don't 
know the correct signature, use the wildcard:

    {*}

If a value can have multiple types, you can list them explicitly, e.g.

    {number|string}
    {false|null|undefined}

### Example (including an `@example` :-):

#### noop.js

    /**
     * Return the passed value.
     *
     * @example
     * myArray.filter(noop);
     * 
     * @param {*} value
     * @return {boolean}
     */
     export const noop = value => value;

#### noop.test.js

    import {noop} from './noop.js';

    /**
     * @test {noop}
     */
     describe('The `noop` function ...', <test function>);

### React `props`

React `props` are documented in two different ways depending on the context.

#### Functional Components

Functional Component `props` are annotated with the regular `@param` tag.

The regular object parameter can have additional `@property` annotations: 

    /**
     * @param {Object} props
     * @property {*} props.children
     * @return {ReactElement}
     */
    function MyComponent (props) => (
      <div>{props.children}</div>
    );

A destructured object parameter must be explicitly annotated as an 
object `@param` with an arbitrary identifier that is used to prefix 
property identifiers. **Destructured properties themselves must be 
annotated with the `@param` tag as well.**

    /**
     * @param {Object} props
     * @param {*} props.children
     * @return {ReactElement}
     */
    function MyComponent ({
      children,
    }) => (
      <div>{children}</div>
    );

#### Class Components

Class Component `props` are annotated with the
[ESDoc React Plugin](https://github.com/esdoc/esdoc-plugins/tree/master/esdoc-react-plugin)
`@reactProps` tag:

    /**
     * @reactProps {*} children
     */
    class MyComponent extends Component {
      render() {
        const { children } = this.props;
      
        /**
         * @return {ReactElement}
         */
        return (
          <div>{children}</div>
        );
      }
    }

### Visibility

API Documentation generators need ECMAScript identifiers for annotated values.
Anonymous default exports of higher order components are a good real life example 
of a documentation anti-pattern:

    /**
     * Some HoC.
     *
     * @type {Function}
     */
    export default someHoc(options)(SomeComponent);

That won't generate anything. The solution is to assign to an identifier first
and then export the identifier:

    /**
     * My HoC.
     *
     * @type {Function}
     */
    const MyHoc = someHoc(options)(SomeComponent);

    export default MyHoc;
