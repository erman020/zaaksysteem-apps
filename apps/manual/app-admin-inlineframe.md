# Admin: `InlineFrame`

> The `InlineFrame` component is the most complex component in the 
  application. Its purpose is to incrementally replace framed legacy
  content with new *React* components.

## First principles

The *React* app lives in a `window` host object. It renders an 
`iframe` **element** that is owned by that window's `document`.

The `iframe` **element** itself has a **content** `window`. 
The **content** window is transient because it is destroyed whenever 
the `src` property of the `iframe` **element** changes, while the 
`iframe` **element** is perpetual in the context of the current view.

That is why the only uniform hook for detecting changes is the
`iframe` **element**'s `load` event.

Note: there is **no** corresponding `unload` event for 
`iframe` **elements**, that's why we listen to its **content window**
for that purpose.

### See also

- https://developer.mozilla.org/en-US/docs/Web/API/Node/ownerDocument
- https://developer.mozilla.org/en-US/docs/Web/API/HTMLIFrameElement/contentWindow

## Route mapping

Resolve the iframe URL or React component.

- `/apps/Admin/View/library/router/base.js`

The router map module entry point. This just enables reading and 
swapping modules.

- `/apps/Admin/View/library/router/base.iframe.js`

The actual production configuration.

The first segment of the top window's path component (i.e. from the 
URL you see in the address bar) is the key for the route.

If its mapped value is a string, subsequent segments and the 
query component (if any) are appended as is to that path.

If its value is not a string, the mapped component is used instead 
of `InlineFrame`.

- `/apps/Admin/View/library/router/base.edge.js`

Overrides the production configuration for development.

See `/node/bin/library/webpack/edge.js` and the
[Router](./app-admin-router.html) manual page.

## Load order

The iframe loading state is managed by the *React* app's store;
its initial value is `true`.

Lifecycle:

1. hide the iframe and display the loader
2. resolve and set the `iframe` **element**'s `src` property 
3. the `iframe` **element**'s `load` event fires
4. inject a `link` element with CSS overrides in the framed document
   (cf. *CSS overrides* below)
5. the `link` element's `load` event fires 
6. hide the loader and show the iframe
7. the `iframe` **element**'s **content window** `unload` event fires 
8. procede to step 1. 

## Navigation types

Navigation can be triggered in two different ways. On a high level,
we want to
 
1. update the URL in the address bar
2. select the matching menu item in the drawer

Note that the legacy page URLs do not have a 
consistent hierarchy. In those cases it is not 
possible to select a menu item for an 'ancestor'.

### Navigation initiated in the *React* app

This is fairly straightforward; in addition to the *React* app 
route change in the top window, the `src` property of the
`iframe` **element** must be updated.

### Navigation initiated in the iframe window

The frame has *two* sources of navigation:

1. transparent: the user activates a hyperlink or submits a form
2. opaque: the user submits a form, the server initiates a redirect
   ("`GET` after `POST`")

Hence, the only opportunity to notify the parent window is when the 
`load` event of the parent window's `iframe` **element** fires.

## Heads up: CSS overrides

Style Sheets in the `zaaksysteem` repository **cannot be changed**
without regressions in legacy pages that are not framed (PIP, Search).

All overrides for the framed admin pages must be put in

- `/apps/Admin/legacy.css`

Note that this is just a static file that is copied to the web server 
document root and not in the scope of the webpack build.

## Heads up: Overlays

The legacy part of the application uses two different implementations
of modal dialogs with a dark overlay, one of them an *AngularJS* 
module, the other based on the *jQuery UI* dialog widget.

- `/frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsPopup.js`
- `/root/tpl/zaak_v1/nl_NL/js/zaaksysteem/ezra_dialog.js`

When they are opened and closed, they call `postMessage` on the parent
window. The React app in the parent window responds by darkening or
resetting its own (background) color(s) accordingly. Note that there is
a delay with the *jQuery UI* widget that cannot be avoided.

Note that the inital implementation did not use `postMessage` from
the `iframe` **window** but used a fine-grained `MutationObserver` 
to intercept DOM changes. This has proven to be a bad idea because

- the two different dialog implmentations cause different DOM mutations
- the `jQuery UI` dialog alone causes different DOM mutations on first 
  and subsequent usage
- the mere presence of the `MutationObserver` caused inexplicable bugs

The overlay state is managed by the *React* app's store.

## Relation with code in the zaaksysteem repository

- `/frontend/index.js`

This is the webpack entry point for the monkey-patched version of
the `/client` GUI (appbar, drawer, floating action button) that is 
used in legacy parts of the application as the `zs-new` module 
dependency.

For the framed pages in the `Admin` app, it additionally defines
the `zs-new-abstract` module that only exposes direct and transitive 
dependencies, but doesn't render the GUI in pages that are framed.

Note that the webpack build for this entry point is not part of the
`gulp` file watcher in `/frontend`, and has to be build manually with 
`npm run webpack` whenever necessary.

- `/frontend/zaaksysteem/src/js/nl/mintlab/_zaaksysteem.js`

This is the entry point for the old AngularJS app. If the page that
loads it is framed, it adds the `zs-new-abstract` dependency, otherwise
`zs-new`.
