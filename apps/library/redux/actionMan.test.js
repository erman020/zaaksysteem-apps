import actionMan from './actionMan';

/**
 * @test {actionMan}
 */
describe('`actionMan`', () => {
  test('maps an Array of strings to an object with action creators', () => {
    const descriptor = ['login', 'logout'];
    const action = actionMan(descriptor);

    expect(typeof action.login).toBe('function');
    expect(typeof action.logout).toBe('function');
  });

  test('produces action creators that yield their action type when toString is called', () => {
    const descriptor = {
      user: ['login', 'logout'],
    };
    const action = actionMan(descriptor);

    expect(String(action.user.login)).toBe('USER:LOGIN');
    expect(String(action.user.logout)).toBe('USER:LOGOUT');
  });
});
