const { assign, keys } = Object;
const { isArray } = Array;

const SEPARATOR = ':';

const transform = segment => segment.replace(/([A-Z])/g, '_$1').toUpperCase();

const getActionType = (key, segments) =>
  segments
    .concat(key)
    .map(transform)
    .join(SEPARATOR);

function actionCreatorFactory(key, segments) {
  const type = getActionType(key, segments);

  const createAction = (payload, error, meta) => ({
    type,
    payload,
    error,
    meta,
  });

  createAction.toString = () => type;

  return createAction;
}

export default function actionMan(descriptor, bucket = {}, segments = []) {
  if (isArray(descriptor)) {
    return descriptor.reduce(
      (accumulator, key) =>
        assign(accumulator, {
          [key]: actionCreatorFactory(key, segments),
        }),
      bucket
    );
  }

  return keys(descriptor).reduce(
    (accumulator, key) =>
      assign(accumulator, {
        [key]: actionMan(
          descriptor[key],
          accumulator[key],
          segments.concat(key)
        ),
      }),
    bucket
  );
}
