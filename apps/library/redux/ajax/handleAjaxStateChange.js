import {
  AJAX_STATE_ERROR,
  AJAX_STATE_PENDING,
  AJAX_STATE_VALID,
} from './createAjaxConstants';

/**
 * Reducer for handling Ajax state changes (pending, valid and error)
 *
 * @param {Object} constants
 * @returns {Function}
 */
export const handleAjaxStateChange = constants => (state, action) => {
  switch (action.type) {
    case constants.PENDING:
      return {
        ...state,
        state: AJAX_STATE_PENDING,
      };

    case constants.SUCCESS:
      return {
        ...state,
        state: AJAX_STATE_VALID,
      };

    case constants.ERROR:
      return {
        ...state,
        state: AJAX_STATE_ERROR,
      };

    default:
      return state;
  }
};

export default handleAjaxStateChange;
