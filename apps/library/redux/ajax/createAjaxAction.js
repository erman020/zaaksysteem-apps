/* eslint-disable valid-jsdoc */
import { request } from '../../fetch';
import { get } from '@mintlab/kitchen-sink';

/**
 * @param {{PENDING: string, ERROR: string, SUCCESS: string}} constants
 * @returns {(options: { method: string, url: string, data: Object, payload: Object, headers: Object})
 *  => (dispatch: Function)
 *    => Promise
 * }
 */
export const createAjaxAction = constants => ({
  method,
  url,
  data,
  payload,
  headers,
}) => dispatch => {
  dispatch({
    type: constants.PENDING,
    ajax: true,
    payload: {
      ...payload,
    },
  });

  return request(method, url, data, headers)
    .then(response => {
      dispatch({
        type: constants.SUCCESS,
        ajax: true,
        payload: {
          ...payload,
          response,
        },
      });
    })
    .catch(error => {
      dispatch({
        type: constants.ERROR,
        ajax: true,
        payload: {
          ...payload,
          error,
        },
        error: true,
        message: get(error, 'message'),
      });
    });
};
