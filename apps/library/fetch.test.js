/**
 * Cf. the JSDOM `fetch` stub in /client.next/test/jest-setup
 */
import { request, getCsrfHeaders, getRequestInit } from './fetch';

const RESOURCE_ID = 'asdf';
const ONE = 1;
const STATUS_OK = 200;
const ANSWER = 42;

const getResponseStub = instance => ({
  result: {
    instance: {
      pager: {},
      rows: [
        {
          instance,
        },
      ],
    },
  },
  status_code: STATUS_OK,
});

const isPromise = value => value instanceof Promise;

/**
 * @test {request}
 */
describe('The `json` module', () => {
  beforeEach(() => {
    window.fetch.stub(getResponseStub(ANSWER));
  });

  test('resolves API response data verbatim', () => {
    const expected = {
      status_code: STATUS_OK,
      result: {
        instance: {
          pager: {},
          rows: [
            {
              instance: ANSWER,
            },
          ],
        },
      },
    };

    expect.assertions(ONE);

    return request('GET', RESOURCE_ID).then(response =>
      expect(response).toEqual(expected)
    );
  });

  test('supports POST', () => {
    expect(isPromise(request('POST', RESOURCE_ID))).toBe(true);
  });

  test('throws if the HTTP method is not supported', () => {
    expect(() => request('FUBAR', RESOURCE_ID)).toThrow();
  });

  describe('has a `getRequestInit` function for the', () => {
    test('GET method', () => {
      const actual = getRequestInit('GET');
      const expected = {
        credentials: 'same-origin',
        headers: {
          Accept: 'application/json',
        },
        method: 'GET',
      };

      expect(actual).toEqual(expected);
    });

    test('POST method', () => {
      const actual = getRequestInit('POST', {
        foo: 42,
      });
      const expected = {
        credentials: 'same-origin',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        method: 'POST',
        body: '{"foo":42}',
      };

      expect(actual).toEqual(expected);
    });
  });

  describe('uses a `getCsrfHeaders` function that', () => {
    afterEach(() => {
      for (const name of ['HEAD', 'TAIL', 'XSRF-TOKEN']) {
        document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 GMT`;
      }
    });

    test('can read an XSRF-TOKEN cookie', () => {
      document.cookie = 'XSRF-TOKEN=1234';

      const actual = getCsrfHeaders();
      const expected = {
        'X-XSRF-TOKEN': '1234',
      };

      expect(actual).toEqual(expected);
    });

    test('can read an XSRF-TOKEN cookie from multiple values', () => {
      document.cookie = 'HEAD=head';
      document.cookie = 'XSRF-TOKEN=1234';
      document.cookie = 'TAIL=tail';

      const actual = getCsrfHeaders();
      const expected = {
        'X-XSRF-TOKEN': '1234',
      };

      expect(actual).toEqual(expected);
    });

    describe('returns `null` if ', () => {
      test('XSRF-TOKEN contains an unsupported character', () => {
        document.cookie = 'XSRF-TOKEN=12,34';

        const actual = getCsrfHeaders();
        const expected = null;

        expect(actual).toEqual(expected);
      });

      test('no XSRF-TOKEN cookie is set', () => {
        document.cookie = 'HEAD=1234';

        const actual = getCsrfHeaders();
        const expected = null;

        expect(actual).toEqual(expected);
      });
    });
  });
});
