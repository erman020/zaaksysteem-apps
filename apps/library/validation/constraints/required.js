import { reduceMap } from '@mintlab/kitchen-sink';
import { isDefined, hasValue } from '../../../library/value';

/**
 * @param {*} userInput
 * @return {boolean}
 */
export const required = userInput => {
  const type = Array.isArray(userInput) ? 'array' : typeof userInput;

  const map = new Map([
    [
      keyType => keyType === 'string',
      () => {
        //ZS-TODO: find a reliable way to do this without document
        let tmp = document.createElement('div');
        tmp.innerHTML = userInput;
        const input = tmp.textContent || tmp.innerText;
        return isDefined(input) && hasValue(input.trim());
      },
    ],
    [
      keyType => keyType === 'array',
      valueUserInput => Boolean(valueUserInput.length),
    ],
    [
      keyType => keyType === 'boolean',
      valueUserInput => hasValue(valueUserInput),
    ],
  ]);

  return reduceMap({
    map,
    keyArguments: [type],
    valueArguments: [userInput],
    fallback: Boolean(userInput),
  });
};
