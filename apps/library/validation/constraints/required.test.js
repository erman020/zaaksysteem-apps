import { required } from './required';

// eslint-disable-next-line
const TEST_ARRAY = [1, 2, 3];

/**
 * @test {required}
 */
describe('The `required` function', () => {
  test('accepts a boolean true value', () => {
    expect(required(true)).toBe(true);
  });

  test('accepts a string value', () => {
    expect(required(' string ')).toBe(true);
  });

  test('correctly handles HTML content', () => {
    expect(required('<strong><i>content</i></strong>')).toBe(true);
    expect(required('<strong><i></i></strong>')).toBe(false);
  });

  test('correctly handles arrays', () => {
    expect(required([])).toBe(false);
    expect(required(TEST_ARRAY)).toBe(true);
  });
});
