/*
 Generic client module for Zaaksysteem API transactions.
 There MUST NOT be knowledge of the Zaaksysteem client *application* here.
 It SHOULD NOT be used in the client *application* directly.
*/
import { dictionary } from '@mintlab/kitchen-sink';

const { assign, keys } = Object;

/**
 * Unquoted RFC 6265 `cookie-value`.
 *
 * @type {string}
 */
const cookieValue = "[\\w!#$%&'()*+./:<=>?@[\\]^`{|}~-]+";

/**
 * @type {RegExp}
 */
const CSRF_EXPRESSION = new RegExp(
  `(?:^|;)\\s?XSRF-TOKEN\\s?=\\s?(${cookieValue})\\s?(?:;|$)`
);

/**
 * Get a custom CSRF HTTP header from the related cookie (if any).
 *
 * CSRF is currently the only scenario where we read a cookie client-side.
 * Libraries (rightfully) deal with the entire historical mess attached
 * to the problem domain, but we only deal with our own API here.
 *
 * @return {Object|null}
 */
export function getCsrfHeaders() {
  const { cookie } = document;
  const matches = CSRF_EXPRESSION.exec(cookie);

  if (matches) {
    const [, backReference] = matches;

    return {
      ['X-XSRF-TOKEN']: decodeURIComponent(backReference),
    };
  }

  return null;
}

/**
 * @type {Object}
 */
const baseInit = {
  credentials: 'same-origin',
  headers: {
    Accept: 'application/json',
  },
};

/**
 * @param {Object} init
 * @return {Object}
 */
export const getInit = init =>
  assign({}, baseInit, init, {
    headers: assign({}, baseInit.headers, init.headers, getCsrfHeaders()),
  });

/***
 * @type {Object}
 */
const methodInitFactories = dictionary({
  /**
   * @param {string} method
   * @return {Object}
   *   `fetch` init parameter Object
   */
  GET(method) {
    return getInit({
      method,
    });
  },
  /**
   * @param {string} method
   * @param {Array|Object} body
   * @return {Object}
   *   `fetch` init parameter Object
   */
  POST(method, body) {
    return getInit({
      method,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });
  },
});

/**
 * @type {Array}
 */
export const methods = keys(methodInitFactories);

/**
 * @param {string} method
 * @param {Array|Object} body
 * @return {RequestInit}
 */
export function getRequestInit(method, body) {
  const factory = methodInitFactories[method];

  if (factory) {
    return factory(method, body);
  }

  throw new Error(`Method '${method}' is not implemented.`);
}

/**
 * @param {Response} response
 * @return {Promise}
 */
function onFulfilled(response) {
  if (response.ok) {
    return response.json();
  }

  return Promise.reject(response.status);
}

/**
 * A network error or CORS misconfiguration
 * results in a TypeError being thrown
 *
 * @param {number|Error} reason
 * @return {Promise}
 *   A promise that is rejected with the error constructor name.
 */
function onRejected(reason) {
  const normalizedReason =
    typeof reason === 'number' ? String(reason) : reason.constructor.name;

  return Promise.reject(normalizedReason);
}

/**
 * Generic HTTP request function for the Zaaksysteem JSON API.
 * Supports GET and POST.
 *
 * @example
 * json('POST', '/api/order', { id: 42 });
 *
 * @param {string} method The request method.
 * @param {string} url The request URL.
 * @param {Array|Object} [body] The request body.
 * @return {Promise}
 */
export const request = (method, url, body) =>
  window
    .fetch(url, getRequestInit(method, body))
    .then(onFulfilled)
    .catch(onRejected);
