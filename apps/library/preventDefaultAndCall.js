import { callOrNothingAtAll } from '@mintlab/kitchen-sink';

/**
 * Executes callback and prevents default event behavior when
 * `altKey`, `ctrlKey`, `shiftKey`, `metaKey` are not pressed
 * @param {Function} callback
 * @return {Function}
 */
export const preventDefaultAndCall = callback => event => {
  const { altKey, ctrlKey, shiftKey, metaKey } = event;

  const shouldPreventDefault = [altKey, ctrlKey, shiftKey, metaKey].every(
    value => value === false
  );

  if (shouldPreventDefault) {
    event.preventDefault();
    callOrNothingAtAll(callback, [event]);
  }
};

export default preventDefaultAndCall;
