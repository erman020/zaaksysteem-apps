import { preventDefaultAndCall } from './preventDefaultAndCall';

const createEventMock = ({
  altKey = false,
  ctrlKey = false,
  shiftKey = false,
  metaKey = false,
} = {}) => ({
  altKey,
  ctrlKey,
  shiftKey,
  metaKey,
  preventDefault: jest.fn(),
});

describe('The `preventDefaultAndCall` function', () => {
  test('executes the callback and prevents default behavior when no key is pressed', () => {
    const callback = jest.fn();
    const event = createEventMock();
    preventDefaultAndCall(callback)(event);

    expect(callback).toBeCalled();
    expect(event.preventDefault).toBeCalled();
  });

  ['altKey', 'ctrlKey', 'shiftKey', 'metaKey'].forEach(key => {
    test(`does not execute the callback and does not prevent default behavior when ${key} is pressed`, () => {
      const callback = jest.fn();
      const event = createEventMock({
        [key]: true,
      });
      preventDefaultAndCall(callback)(event);
      expect(callback).not.toBeCalled();
      expect(event.preventDefault).not.toBeCalled();
    });
  });
});
