/**
 * Get the combined path and query components
 * from a window's location object.
 *
 * @param {Window} [contextWindow = window]
 * @return {string}
 */
export function getUrl(contextWindow = window) {
  const { href } = contextWindow.location;

  return parseUrl(href);
}

/**
 * @param {string} url
 * @param {Window} [contextWindow=window]
 */
export function navigate(url, contextWindow = window) {
  contextWindow.location.assign(url);
}

/**
 * Get the combined path and query components from a URI.
 *
 * @param {string} uri
 * @return {string}
 */
export function parseUrl(uri) {
  const { pathname, search } = new URL(uri);

  return [pathname.replace(/\/$/, ''), search].join('');
}
