export const locale = {
  nl: {
    aria: {
      adminIframe: 'Beheer formulier',
      mainMenu: 'Hoofdmenu',
      userMenu: 'Gebruikers menu',
    },
    attributes: {
      types: {
        bag_openbareruimte: 'Straat',
        bag_openbareruimtes: 'Straten',
        bag_straat_adres: 'Adres (dmv straatnaam)',
        bag_straat_adressen: 'Adressen (dmv straatnaam)',
        bag_adres: 'Adres (dmv postcode)',
        bag_adressen: 'Adressen (dmv postcode)',
        bankaccount: 'Rekeningnummer',
        appointment: 'Kalenderafspraak',
        calendar: 'Kalenderafspraak (QMatic)',
        calendar_supersaas: 'Kalenderafspraak (SuperSaaS)',
        checkbox: 'Meervoudige keuze',
        date: 'Datum',
        email: 'E-mail',
        file: 'Document',
        geolatlon: 'Locatie met kaart',
        googlemaps: 'Adres (Google Maps)',
        image_from_url: 'Afbeelding',
        numeric: 'Numeriek',
        option: 'Enkelvoudige keuze',
        richtext: 'Rich text',
        select: 'Keuzelijst',
        subject: 'Betrokkene',
        text: 'Tekstveld',
        text_uc: 'Tekstveld (HOOFDLETTERS)',
        textarea: 'Groot tekstveld',
        url: 'Webadres',
        valuta: 'Valuta',
        valutaex: 'Valuta (exclusief BTW)',
        valutaex6: 'Valuta (exclusief BTW (6))',
        valutaex21: 'Valuta (exclusief BTW (21))',
        valutain: 'Valuta (inclusief BTW)',
        valutain6: 'Valuta (inclusief BTW (6))',
        valutain21: 'Valuta (inclusief BTW (21))',
      },
    },
    common: {
      title: 'Zaaksysteem.nl',
      admin: 'Beheer',
      routeNotFound: 'De pagina kan niet worden gevonden',
      dates: {
        dayNamesShort: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
        dayNames: [
          'Zondag',
          'Maandag',
          'Dinsdag',
          'Woensdag',
          'Donderdag',
          'Vrijdag',
          'Zaterdag',
        ],
        monthNamesShort: [
          'Jan',
          'Feb',
          'Mrt',
          'Apr',
          'Mei',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Okt',
          'Nov',
          'Dec',
        ],
        monthNames: [
          'Januari',
          'Februari',
          'Maart',
          'April',
          'Mei',
          'Juni',
          'Juli',
          'Augustus',
          'September',
          'Oktober',
          'November',
          'December',
        ],
        DoFn: month => month,
      },
    },
    config: {
      saved: 'De configuratie-items zijn succesvol opgeslagen.',
    },
    navigation: {
      about: 'Over zaaksysteem.nl',
      handle: 'Behandelen',
      logout: 'Uitloggen',
      support: 'Help',
    },
    validation: {
      required: 'Voer een geldige waarde in.',
      number: 'Voer een numerieke waarde in.',
      email: 'Voer een geldig e-mailadres in.',
      uri: 'Voer een geldige URL in.',
    },
    server: {
      status403: 'U heeft niet genoeg rechten om deze actie uit te voeren.',
      status500:
        'Er is een fout op de server opgetreden. Probeer het later opnieuw.',
    },
    dialog: {
      ok: 'OK',
      cancel: 'Annuleren',
      discardChanges: {
        title: 'Wijzigingen aangebracht',
        text:
          'Er zijn wijzigingen aangebracht die nog niet zijn opgeslagen. Wilt u doorgaan?',
      },
    },
    form: {
      loading: 'Bezig met laden…',
      choose: 'Maak een keuze.',
      beginTyping: 'Typ om te zoeken…',
      creatable: 'Typ, en <ENTER> om te bevestigen.',
      create: 'Aanmaken:',
      errorsInForm: 'Er zitten fouten in één of meerdere configuratie-items.',
      undo: 'Ongedaan maken',
      unsavedChanges: 'Er zijn onopgeslagen configuratie-items.',
      save: 'Opslaan',
    },
    catalog: {
      actions: {
        import: 'Importeren',
      },
      column: {
        icon: '',
        name: 'Naam',
        type: 'Type',
      },
      detailView: {
        enclosedDocument: 'Ingesloten document',
        itemsSelected: 'onderdelen geselecteerd',
        lastModified: 'Gewijzigd',
        valueFallback: 'Onbekend',
        magicString: 'Magicstring',
        used_in_case_types: 'Zaaktypen',
        used_in_object_types: 'Objecttypen',
        identification: 'Identificatie',
        id: 'UUID',
        document: 'Document',
        valueType: 'Invoertype',
        versionTitle: 'Versie',
        relationsTitle: 'Gebruik',
      },
      title: 'Catalogus',
      type: {
        attribute: 'Kenmerk',
        case_type: 'Zaaktype',
        document_template: 'Documentsjabloon',
        email_template: 'E-mailsjabloon',
        folder: 'Map',
        object_type: 'Objecttype',
      },
    },
    log: {
      column: {
        caseId: 'Zaak #',
        date: 'Datum',
        description: 'Gebeurtenis',
        component: 'Component',
        user: 'Behandelaar',
      },
      components: {
        api: 'Api',
        authentication: 'Authenticatie',
        berichtenbox: 'Berichtenbox',
        betrokkene: 'Betrokkene',
        case: 'Zaak',
        document: 'Document',
        documenten: 'Document',
        email: 'E-mail',
        event: 'Gebeurtenis',
        interface: 'Interface',
        kenmerk: 'Kenmerk',
        notificatie: 'Notificatie',
        Sim: 'Simloket',
        sjabloon: 'Sjabloon',
        sysin: 'Systeem',
        transaction: 'Transactie',
        user: 'Gebruiker',
        woz_objects: 'WOZ object',
        zaak: 'Zaak',
        zaaktype: 'Zaaktype',
      },
      exportButtonTitle: 'Exporteer als CSV',
      filter: {
        placeholder: {
          keyword: 'Filter op trefwoord',
          caseNumber: 'Zaak #',
          user: 'Behandelaar',
        },
        beginTyping: 'Begin met typen…',
        loading: 'Laden…',
      },
      labelRowsPerPage: 'Per pagina:',
      title: 'Logboek',
      noResultDescription: 'Geen resultaten gevonden.',
    },
    systemConfiguration: {
      title: 'Configuratie',
      categories: {
        cases: 'Zaken',
        users: 'Gebruikers',
        pip: 'PIP',
        documents: 'Documenten',
        about: 'Over',
        premium: 'Premium',
        other: 'Overig',
        deprecated: 'Uitgefaseerd',
      },
      titles: {
        notifications: 'Notificaties',
        emailTemplate: 'E-mailsjabloon titel',
        rejectedCases: 'Afgewezen zaken',
        newUsers: 'Nieuwe gebruikers',
        confirmationNewUsers: 'Bevestigingse-mail nieuwe gebruikers',
        dashboard: 'Dashboard',
        signatureUploadRole: 'Handtekening uploaden',
        pipFeedback: 'Verzenden PIP-feedback',
        pipIntro: 'Welkomsttekst PIP',
        documentIntake: 'Documentinname zonder gebruiker',
        documentRecognition: 'Documentherkenning gebruiker',
        jodConverter: 'Jodconverter',
        customerInfo: 'Informatie',
        customerAddress: 'Adresgegevens',
        customerExtra: 'Aanvullende adresgegevens',
        customerPhone: 'Telefoonnummers',
        customerMail: 'E-mail',
        documentWatcher: 'Zaaksysteem document watcher',
        bagPriority: 'Gemeentes met voorrang bij BAG-zoekopdrachten',
        publicManpage: 'API handleiding Zaaksysteem.nl',
        customRelationRoles: 'Extra rollen voor betrokkenen',
        location: 'Weergave vestigingslocatie',
        pipAuthorization: 'E-mailnotificatie machtiging betrokkene',
        externalSearch: 'Aanvrager externe zoekfunctie',
        changePassword: 'Gebruikerswachtwoord',
        stufZkn: 'StUF-ZKN',
        pdfAnnotations: 'Annotaties',
        wordApp: 'De MS Word app is geactiveerd voor de omgeving',
        allowedTemplates: 'Toegestane web-sjablonen',
        customerInfoGemeente: "Overige URL's",
      },
      descriptions: {
        notifications:
          'Selecteer het e-mailsjabloon dat standaard wordt verstuurd wanneer de behandelaar wijzigt.',
        rejectedCases:
          'Stuur afgewezen zaken door naar een aangewezen afdeling en rol.',
        confirmationNewUsers: 'Bevestigingse-mail nieuwe gebruikers',
        signatureUploadRole:
          'Selecteer wie een handtekening mag uploaden voor de e-mails.',
        externalSearch:
          'Titel en link zijn pas relevant wanneer de extra zoekknop in het aanvrager dialoog zichtbaar is.',
      },
      help: {
        allocation_notification_template_id:
          'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij een toewijzing wijziging op een zaak.',
        case_distributor_group:
          'Wanneer een zaak wordt afgewezen, wordt deze doorgestuurd naar de geconfigureerde afdeling en rol.',
        case_distributor_role:
          'Wanneer een zaak wordt afgewezen, wordt deze doorgestuurd naar de geconfigureerde afdeling en rol.',
        first_login_confirmation:
          'Deze tekst verschijnt bij een nieuwe gebruiker, wanneer hij/zij haar afdeling heeft geselecteerd.',
        first_login_intro:
          'Deze tekst verschijnt wanneer een gebruiker voor het eerst inlogt in Zaaksysteem.',
        new_user_template:
          'Deze instelling wijzigt het e-mailsjabloon welke verzonden wordt bij het accepteren van een nieuwe gebruiker.',
        disable_dashboard_customization:
          'Standaard is het mogelijk om dashboard-widgets te verplaatsen, toe te voegen en te verwijderen. Als deze instelling aan staat, is deze mogelijkheid er niet.',
        signature_upload_role:
          'Deze instelling geeft aan welke rol rechten heeft om handtekeningen te uploaden.',
        feedback_email_template_id:
          'Deze instelling wijzigt het sjabloon dat gebruikt wordt bij het verzenden van feedback uit de PIP.',
        pip_login_intro:
          'Deze tekst verschijnt op de inlogpagina van de PIP. Indien er geen waarde is ingevuld wordt er geen tekst weergegeven.',
        file_username_seperator:
          'Dit is het scheidingsteken waarmee de gebruiker van de bestandsnaam onderscheiden wordt.',
        document_intake_user:
          'Dit is de virtuele gebruiker waarnaar documenten ge-e-maild kunnen worden die niet direct aan een gebruiker toegewezen moeten of kunnen worden. Dit mag niet matchen met een echte user in LDAP!',
        customer_info_website: 'De URL van uw website.',
        customer_info_naam:
          'De naam van uw organisatie. Deze naam is ook te zien in de URL-balk.',
        customer_info_naam_lang:
          'Deze naam wordt onder meer gebruikt indien er een zaakregistratie heeft plaatsgevonden.',
        customer_info_naam_kort:
          'Deze naam wordt onder meer gebruikt voor e-mailfunctionaliteiten.',
        customer_info_straatnaam: 'De straatnaam van uw vestiging.',
        customer_info_huisnummer: 'Het huisnummer van uw vestiging.',
        customer_info_postcode: 'De postcode van uw vestiging.',
        customer_info_woonplaats: 'De vestigingsplaats van uw organisatie.',
        customer_info_postbus: 'De postbus van uw vestiging.',
        customer_info_postbus_postcode:
          'De postcode van uw postbus van uw vestiging.',
        customer_info_telefoonnummer: 'Het telefoonnummer van uw vestiging.',
        customer_info_faxnummer: 'Het faxnummer van uw vestiging.',
        customer_info_zaak_email:
          "Het e-mailadres voor uw zaakcorrespondentie. Deze optie wordt uitgefaseerd, gebruik de 'Uitgaande e-mailkoppeling in het koppelingsoverzicht'.",
        customer_info_email: 'Uw algemene e-mailadres.',
        customer_info_gemeente_id_url:
          'Deze URL wordt gebruikt voor het aanvragen van een bedrijven-ID, zoals bijvoorbeeld het mintlabID. Deze wordt steeds minder gebruikt vanwege eHerkenning.',
        customer_info_gemeente_portal:
          'De URL van uw producten en dienstenpagina, deze wordt gebruikt indien een klant een aanvraag annuleert.',
        files_locally_editable:
          'Zet de functionaliteit voor het lokaal bewerken van documenten aan (Zaaksysteem document watcher).',
        bag_priority_gemeentes:
          'Lijst van gemeentes die voorrang moeten krijgen bij het zoeken naar adressen.',
        public_manpage:
          'Deze instelling bepaalt of de /man van uw zaaksysteem publiekelijk beschikbaar is.',
        custom_relation_roles:
          'Extra namen voor rollen van betrokkenen (bovenop de ingebouwde NEN-lijst).',
        customer_info_latitude:
          'De breedtegraad van uw vestiging. Voor gebruik van de kaartfunctionaliteiten.',
        customer_info_longitude:
          'De lengtegraad van uw vestiging. Voor gebruik van de kaartfunctionaliteiten.',
        subject_pip_authorization_confirmation_template_id:
          'Deze instelling wijzigt het sjabloon dat gebruikt wordt om e-mailnotificaties te versturen bij een betrokkene machtiging op een zaak.',
        requestor_search_extension_active:
          'Deze instelling schakelt de extra zoekknop in het aanvrager dialoog in.',
        requestor_search_extension_name:
          'Voer hier de knoptitel in voor een externe link die op de aanvrager zoek-popup dialog verschijnt.',
        requestor_search_extension_href:
          'Voer het webadres in voor een externe link die op de aanvrager zoek-popup dialog verschijnt.',
        users_can_change_password:
          'Dit werkt alleen voor handmatig aangemaakte gebruikers, niet voor gebruikers die via ADFS zijn aangemaakt.',
        enable_stufzkn_simulator:
          'StUF-ZKN-simulator simuleert een ZSC, en kan gebruikt worden om handmatig StUF-ZKN berichten te sturen naar een StUF-ZKN ZS.',
        pdf_annotations_public:
          'Deze instelling bepaalt welke annotaties verschijnen in de PDF-voorbeeldweergave.',
        allowed_templates: 'Sjablonen die toegestaan zijn voor /form.',
      },
    },
  },
};

export default locale;
