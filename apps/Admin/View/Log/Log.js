import React, { Component } from 'react';
import { logStyleSheet } from './Log.style';
import LogHeader from './header/LogHeader';
import LogTable from './table/LogTable';
import { sharedStylesheet } from '../Shared/Shared.style';
import { withStyles, Pagination, Loader, Card } from '@mintlab/ui';
import { get } from '@mintlab/kitchen-sink';
import deepmerge from 'deepmerge';

/**
 * @reactProps {Object} classes
 * @reactProps {string} caseNumberTranslation
 * @reactProps {Array} columns
 * @reactProps {number} count
 * @reactProps {string} exportButtonTitle
 * @reactProps {Function} exportLog
 * @reactProps {Function} fetchUsers
 * @reactProps {Object} keywordTranslation
 * @reactProps {string} headerTitle
 * @reactProps {string} labelRowsPerPage
 * @reactProps {number} page
 * @reactProps {number} rowsPerPage
 * @reactProps {Array<Object>} rows
 * @reactProps {Array<number>} rowsPerPageOptions
 * @reactProps {Array<Object>} userOptions *
 * @reactProps {Object} userTranslations
 * @reactProps {Object} values
 * @reactProps {Boolean} loading
 * @reactProps {boolean} userOptionsLoading
 * @reactProps {Boolean} showFilters
 */
export class Log extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    const { showFilters } = props;

    this.state = {
      focusedFilter: undefined,
      showFilters,
    };
  }

  /**
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        classes,
        caseNumberTranslation,
        columns,
        count,
        exportButtonTitle,
        exportLog,
        fetchUsers,
        keywordTranslation,
        headerTitle,
        labelRowsPerPage,
        page,
        rowsPerPage,
        rows,
        rowsPerPageOptions,
        t,
        userOptions,
        userTranslations,
        values,
        loading,
        userOptionsLoading,
      },
      state: { showFilters, focusedFilter },
    } = this;

    return (
      <div className={classes.wrapper}>
        <LogHeader
          caseNumberTranslation={caseNumberTranslation}
          exportButtonTitle={exportButtonTitle}
          exportLog={exportLog}
          fetchUsersList={fetchUsers}
          headerTitle={headerTitle}
          keywordTranslation={keywordTranslation}
          userTranslations={userTranslations}
          userOptions={userOptions}
          showFilters={showFilters}
          focusedFilter={focusedFilter}
          changeFocus={this.changeFocus}
          removeFocus={this.removeFocus}
          onTextFieldKeyDown={this.onTextFieldKeyDown}
          clearFilter={this.clearFilter}
          toggleFilters={this.toggleFilters}
          values={values}
          handleChange={this.handleFieldChange}
          setFieldValue={this.props.setFieldValue}
          userOptionsLoading={userOptionsLoading}
        />

        <div className={classes.sheet}>
          <Card className={classes.tableWrapper}>
            {loading ? (
              <Loader scope="log-loader" />
            ) : (
              <LogTable
                classes={classes}
                columns={columns}
                page={page}
                rows={rows}
                t={t}
              />
            )}
          </Card>
          <Pagination
            changeRowsPerPage={this.changeRowsPerPage}
            component={'div'}
            count={count}
            getNewPage={this.getNewPage}
            labelDisplayedRows={() => {}}
            labelRowsPerPage={labelRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={rowsPerPageOptions}
          />
        </div>
      </div>
    );
  }

  /**
   * @param {number} nextPage
   *   Synthetic event.
   */
  getNewPage = nextPage => {
    this.props.setPage(nextPage + 1);
  };

  /**
   * @param {Object} event
   *   Synthetic event.
   */
  changeRowsPerPage = event => {
    const nextRowsPerPage = get(event, 'target.value');
    this.props.setRowsPerPage(nextRowsPerPage);
  };

  /**
   * @param {Object} values
   */
  applyValuesAsFilters = values => {
    const { applyFilters } = this.props;

    applyFilters(values);
  };

  /**
   * @param {Object} keyPressEvent
   *   Synthetic event.
   */
  onTextFieldKeyDown = keyPressEvent => {
    if (keyPressEvent.key === 'Enter') {
      this.applyValuesAsFilters(this.props.values);
    }
  };

  /**
   * @param {string} name
   *   Synthetic event.
   */
  clearFilter = name => {
    const { setFieldValue, formDefaults, values } = this.props;

    setFieldValue(name, formDefaults[name]);

    this.applyValuesAsFilters({
      ...values,
      [name]: null,
    });
  };

  /**
   *
   * @param {Object} event
   *   Synthetic event.
   */
  handleFieldChange = event => {
    this.props.handleChange(event);

    if (event.target.type === 'select') {
      this.applyValuesAsFilters({
        ...this.props.values,
        user: {
          ...event.target.value,
        },
      });
    }
  };

  /**
   * Toggles the filter bar and resets the filters to default
   */
  toggleFilters = () => {
    const nextState = !this.state.showFilters;
    this.setState({
      showFilters: nextState,
    });

    if (nextState === false) {
      this.props.resetForm(this.props.formDefaults);
      this.props.clearFilters();
    }
  };

  /**
   * @param {SyntheticEvent} event
   */
  changeFocus = event => {
    this.setState({
      focusedFilter: get(event, 'target.name'),
    });
  };

  removeFocus = () => {
    this.setState({
      focusedFilter: undefined,
    });
  };
}

/**
 * @param {Object} theme
 * @return {JSS}
 */
const mergedStyles = theme =>
  deepmerge(sharedStylesheet(theme), logStyleSheet(theme));

export default withStyles(mergedStyles)(Log);
