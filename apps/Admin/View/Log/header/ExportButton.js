import React from 'react';
import classNames from 'classnames';
import { exportButtonStyleSheet } from './ExportButton.style';
import { withStyles, Button } from '@mintlab/ui';

/**
 * @reactProps {Object} classes
 * @reactProps {string} baseClassName
 * @reactProps {Function} action
 * @reactProps {Object} exportParams
 * @reactProps {string} value
 * @return {ReactElement}
 */
export const ExportButton = ({ classes, baseClassName, action, value }) => (
  <div className={classNames(baseClassName, classes.wrapper)}>
    <Button action={() => action()} presets={['secondary', 'contained']}>
      {value}
    </Button>
  </div>
);

export default withStyles(exportButtonStyleSheet)(ExportButton);
