import React, { createElement } from 'react';
import { filterStyleSheet } from './Filter.style';
import { withStyles, Icon, Select } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.changeFilterValue
 * @param {Function} props.changeFocus
 * @param {Function} props.removeFocus
 * @param {Boolean} props.hasFocus
 * @param {string} props.name
 * @param {string} props.value
 * @param {string} props.startAdornmentName
 * @param {Object} props.userTranslations
 * @param {Function} props.fetchUsersList
 * @param {Array<Object>} props.userOptions
 * @param {Function} onChange
 * @return {ReactElement}
 */
export const TextFieldFilter = ({
  classes,
  changeFocus,
  removeFocus,
  hasFocus,
  name,
  value,
  startAdornmentName,
  userTranslations,
  fetchUsersList,
  userOptions,
  onChange,
  loading,
}) => (
  <div className={classes.filterWrapper}>
    <Select
      name={name}
      value={value}
      choices={userOptions}
      generic={true}
      getChoices={fetchUsersList}
      hasFocus={hasFocus}
      onChange={onChange}
      onFocus={changeFocus}
      onBlur={removeFocus}
      translations={userTranslations}
      startAdornment={createElement(Icon, {
        size: 'small',
        children: startAdornmentName,
      })}
      filterOption={option => option}
      isClearable={true}
      loading={loading}
    />
  </div>
);

export default withStyles(filterStyleSheet)(TextFieldFilter);
