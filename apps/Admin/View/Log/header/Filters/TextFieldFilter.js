import React, { createElement } from 'react';
import { filterStyleSheet } from './Filter.style';
import { withStyles, GenericTextField, Icon } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.name
 * @param {string} props.value
 * @param {Boolean} props.hasFocus
 * @param {string} props.startAdornmentName
 * @param {Function} props.endAdornmentAction
 * @param {string} props.placeholder
 * @param {Function} props.onChange
 * @param {Function} props.onKeyPress
 * @param {Function} props.onFocus
 * @param {Function} props.onBlur
 * @return {ReactComponent}
 */
export const TextFieldFilter = ({
  classes,
  name,
  value,
  hasFocus,
  startAdornmentName,
  endAdornmentAction,
  placeholder,
  onChange,
  onKeyPress,
  onFocus,
  onBlur,
}) => (
  <div className={classes.filterWrapper}>
    <GenericTextField
      name={name}
      value={value}
      hasFocus={hasFocus}
      startAdornment={createElement(Icon, {
        size: 'small',
        children: startAdornmentName,
      })}
      closeAction={value ? () => endAdornmentAction(name) : null}
      placeholder={placeholder}
      onChange={onChange}
      onKeyPress={onKeyPress}
      onFocus={onFocus}
      onBlur={onBlur}
      scope={`log-filter:${name}`}
    />
  </div>
);

export default withStyles(filterStyleSheet)(TextFieldFilter);
