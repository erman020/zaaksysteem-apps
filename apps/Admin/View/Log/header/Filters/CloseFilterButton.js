import React from 'react';
import { Button } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Function} props.action
 * @param {String} props.type
 * @return {ReactElement}
 */
export const CloseFilterButton = ({ action, type }) => (
  <Button action={action} presets={['icon', 'extraSmall']}>
    {type}
  </Button>
);

export default CloseFilterButton;
