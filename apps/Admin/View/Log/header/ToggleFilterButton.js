import React from 'react';
import { Button } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Function} props.action
 * @param {string} props.type
 * @return {ReactElement}
 */
export const ToggleFilterButton = ({ action, type }) => (
  <Button
    action={action}
    presets={['icon', 'medium']}
    scope={`toggle-filter:${type}`}
  >
    {type}
  </Button>
);

export default ToggleFilterButton;
