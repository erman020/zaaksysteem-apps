import React from 'react';
import { Render } from '@mintlab/ui';
import { cellStyleSheet } from './cells.style';
import ColoredIcon from './../../Icons/ColoredIcon';
import { withStyles } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.value
 * @return {ReactElement}
 */
export const IconCell = ({ value }) => (
  <Render condition={value}>
    <ColoredIcon size="large" value={value} />
  </Render>
);

export default withStyles(cellStyleSheet)(IconCell);
