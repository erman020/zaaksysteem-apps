import React from 'react';
import classNames from 'classnames';
import { cellStyleSheet } from './cells.style';
import { withStyles } from '@mintlab/ui';
import { withFormatDate } from './../../App/withFormatDate';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.formatDate
 * @param {string} props.value
 * @return {ReactElement}
 */
export const DateCell = ({ classes, formatDate, value }) => {
  const date = new Date(value);

  return (
    <div className={classes.dateTime}>
      <span className={classNames(classes.dateTimeWrapper, classes.date)}>
        {formatDate(date, 'YYYY-MM-DD')}
      </span>
      <span className={classNames(classes.dateTimeWrapper, classes.time)}>
        {formatDate(date, 'hh:mm:ss')}
      </span>
    </div>
  );
};

export default withStyles(cellStyleSheet)(withFormatDate(DateCell));
