import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import Layout from './Layout';
import {
  openDrawer,
  closeDrawer,
  openOverlay,
  closeOverlay,
  loadWindow,
  unloadWindow,
} from '../../../Store/UI/ui.actions';
import { logout } from '../../../Store/Session/session.actions';

const mapStateToProps = ({
  ui: {
    drawer,
    iframe: { loading, overlay },
    banners,
  },
  route,
}) => ({
  hasIframeOverlay: overlay,
  isDrawerOpen: drawer,
  isIframeLoading: loading,
  requestUrl: route,
  banners,
});

const mapDispatchToProps = dispatch => ({
  logout: payload => dispatch(logout(payload)),
  closeDrawer: payload => dispatch(closeDrawer(payload)),
  openDrawer: payload => dispatch(openDrawer(payload)),
  onIframeOpen: () => dispatch(openOverlay()),
  onIframeClose: () => dispatch(closeOverlay()),
  onIframeLoad: () => dispatch(loadWindow()),
  onIframeUnload: () => dispatch(unloadWindow()),
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    toggleDrawer() {
      if (stateProps.isDrawerOpen) {
        dispatchProps.closeDrawer();
      } else {
        dispatchProps.openDrawer();
      }
    },
  };
}

const connectWithTranslation = translate();
const connectWithStore = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
);

/**
 * The Layout Container Component is connected
 * with the store and the translations.
 *
 * @type {Function}
 */
const LayoutContainer = connectWithTranslation(connectWithStore(Layout));

export default LayoutContainer;
