import { createElement } from 'react';
import ErrorNotFound from '../ErrorNotFound';
import getComponent from '../../library/router/getComponent';

const IFRAME_OVERLAY_CLASS = 'iframe-overlay';

function setLegacyBodyClass(hasOverlay) {
  if (hasOverlay) {
    document.body.classList.add(IFRAME_OVERLAY_CLASS);
  } else {
    document.body.classList.remove(IFRAME_OVERLAY_CLASS);
  }
}

/**
 * The `View` component is consumed by {@link AdminLayout}
 * and overloaded with one of
 *
 * - {@link ErrorNotFound} (no route found)
 * - {@link InlineFrameLoader} (legacy server-side content)
 * - Original React implementation
 *
 * @reactProps {boolean} hasIframeOverlay
 * @reactProps {boolean} isIframeLoading
 * @reactProps {Function} onIframeClose
 * @reactProps {Function} onIframeOpen
 * @reactProps {Function} onIframeLoad
 * @reactProps {Function} onIframeUnload
 * @reactProps {string} requestUrl
 * @reactProps {Function} route
 * @return {ReactElement}
 */
export default function View({
  hasIframeOverlay,
  isIframeLoading,
  onIframeClose,
  onIframeOpen,
  onIframeLoad,
  onIframeUnload,
  requestUrl,
  route,
  t,
}) {
  const resolvedRoute = getComponent(requestUrl);

  if (!resolvedRoute) {
    return createElement(ErrorNotFound);
  }

  const [component, data, params] = resolvedRoute;

  if (typeof data === 'string') {
    const inlineFrameLoader = createElement(component, {
      hasOverlay: hasIframeOverlay,
      loading: isIframeLoading,
      onOverlayClose: onIframeClose,
      onOverlayOpen: onIframeOpen,
      onLoad: onIframeLoad,
      onUnload: onIframeUnload,
      route,
      t,
      url: requestUrl,
    });

    setLegacyBodyClass(hasIframeOverlay);

    return inlineFrameLoader;
  }

  const routeComponent = createElement(component, {
    route,
    segments: data,
    params,
  });

  return routeComponent;
}
