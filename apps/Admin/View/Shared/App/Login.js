import { createElement } from 'react';
import LayoutContainer from './LayoutContainer';
import { getSegment } from '@mintlab/kitchen-sink';
import { getUserNavigation } from '../../../../library/auth';
import { getUrl } from '../../../../library/url';

/**
 * @param {Object} props
 * @param {Array} props.navigation,
 * @param {Function} props.route,
 * @param {Object} props.session,
 * @return {ReactElement|null}
 */
export default function Login({ navigation, route, session }) {
  const url = getUrl();
  const {
    account: {
      instance: { company: customer },
    },
    logged_in_user: { capabilities, initials, surname },
  } = session;

  const company = customer;
  const userName = `${initials} ${surname}`;
  const userNavigation = getUserNavigation(navigation, capabilities);

  if (!getSegment(url)) {
    const [{ path }] = userNavigation;

    route({
      path,
    });
  }

  return createElement(LayoutContainer, {
    company,
    route,
    userName,
    userNavigation,
  });
}
