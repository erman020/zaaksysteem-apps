import React, { Component } from 'react';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import LoginContainer from './LoginContainer';
import FormatDateProvider from './FormatDate';
import { Loader } from '@mintlab/ui';

const LANGUAGE = 'nl-NL';
const FALLBACK_LANGUAGE = 'en-US';

/**
 * @reactProps {Object} locale
 * @reactProps {Object} session
 */
export default class Locale extends Component {
  constructor(props) {
    super(props);
    this.initialize(props.locale);
  }

  /**
   * @return {ReactElement}
   */
  render() {
    const { session, navigation, bootstrap } = this.props;

    //ZS-TODO: create different views for
    //bootstrap pending and error

    if (bootstrap !== 'valid') {
      return <Loader />;
    }

    return (
      <I18nextProvider i18n={i18n}>
        <FormatDateProvider>
          <LoginContainer navigation={navigation} session={session} />
        </FormatDateProvider>
      </I18nextProvider>
    );
  }

  /**
   * @param {Object} resources
   */
  initialize(resources) {
    i18n.init({
      lng: LANGUAGE,
      fallbackLng: FALLBACK_LANGUAGE,
      resources,
      debug: false,
      react: {
        wait: true,
      },
    });
  }
}
