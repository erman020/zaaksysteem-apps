import React, { Fragment } from 'react';
import SnackbarContainer from '../Snackbar/SnackbarContainer';
import DialogContainer from '../Dialog/DialogContainer';

/**
 * @param {Object} props
 * @param {Object} props.children
 * @return {ReactElement}
 */
const ContainersWrapper = ({ children }) => (
  <Fragment>
    {children}
    <DialogContainer />
    <SnackbarContainer />
  </Fragment>
);

export default ContainersWrapper;
