import { connect } from 'react-redux';
import Login from './Login';
import { invoke } from '../../../Store/Route/route.actions';
import { login } from '../../../Store/Session/session.actions';

const mapDispatchToProps = dispatch => ({
  route(path) {
    dispatch(invoke(path));
  },
  login: payload => dispatch(login(payload)),
});

const LoginContainer = connect(
  null,
  mapDispatchToProps
)(Login);

export default LoginContainer;
