/*
 * App entry component.
 */
import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { MaterialUiThemeProvider } from '@mintlab/ui';
import ErrorBoundary from './ErrorBoundary';
import LocaleContainer from './LocaleContainer';
import { createStore } from '../../../Store';
import { onPopState } from '../../../../library/dom/history';
import { getUrl } from '../../../../library/url';
import { resolve } from '../../../Store/Route/route.actions';
import { init } from '../../../Store/App/app.actions';

const initialState = {};
const store = createStore(initialState);

function dispatchRoute() {
  store.dispatch(
    resolve({
      path: getUrl(),
    })
  );
}

store.dispatch(init());

onPopState(dispatchRoute);

const Provider = () => (
  <StoreProvider store={store}>
    <MaterialUiThemeProvider>
      <ErrorBoundary>
        <LocaleContainer />
      </ErrorBoundary>
    </MaterialUiThemeProvider>
  </StoreProvider>
);

export default Provider;
