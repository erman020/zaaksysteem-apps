import { connect } from 'react-redux';
import Locale from './Locale';
import { navigation } from '../../../Fixtures/navigation';
import { locale } from '../../../Fixtures/locale';

const mapStateToProps = ({
  session: { data: session },
  app: { bootstrap },
}) => ({
  session,
  bootstrap,
});
const mergeProps = stateProps => ({
  ...stateProps,
  navigation,
  locale,
});
const LocaleContainer = connect(
  mapStateToProps,
  null,
  mergeProps
)(Locale);

export default LocaleContainer;
