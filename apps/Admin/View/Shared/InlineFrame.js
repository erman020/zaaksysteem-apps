import React, { Component, createRef } from 'react';
import { getIframeUrl, getParentUrl } from '../library/router/iframe';
import { getUrl, navigate, parseUrl } from '../../../library/url';
import { LOGIN_PATH, login } from '../../../library/auth';
import './InlineFrame.css';

/**
 * InlineFrame component for seamless integration of
 * server-side legacy content in the React admin app.
 * Consumed by {@link InlineFrameLoader}.
 *
 * @reactProps {string} url
 * @reactProps {Function} onLoad
 * @reactProps {Function} onUnload
 * @reactProps {Function} onOverlayOpen
 * @reactProps {Function} onOverlayClose
 */
export default class InlineFrame extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.registerPostMessages();

    /**
     * @type {RefObject}
     */
    this.iframeElement = createRef();
    /**
     * @type {string}
     */
    this.basePath = this.getBasePath();
  }

  /**
   *  @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    this.manageNativeEvents('add');
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#shouldcomponentupdate
   *
   * @param {Object} nextProps
   * @param {string} nextProps.url
   * @return {boolean}
   */
  shouldComponentUpdate({ url }) {
    const { current } = this.iframeElement;
    const { contentWindow, src } = current;
    const currentIframeUrl = getUrl(contentWindow);
    const nextIframeUrl = getIframeUrl(url, this.basePath);

    if (this.isIframeStale(url, nextIframeUrl, currentIframeUrl)) {
      return false;
    }

    // The iframe location must be updated programmatically if the
    // last navigation before navigation was initiated within the iframe
    // is the same as the first navigation afterwards.
    if (this.isIframeOutOfSync(currentIframeUrl, nextIframeUrl, src)) {
      navigate(src, contentWindow);

      return false;
    }

    return true;
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#componentwillunmount
   */
  componentWillUnmount() {
    this.manageNativeEvents('remove');
  }

  /**
   * @return {ReactElement}
   */
  render() {
    const {
      props: { t, url },
    } = this;

    return (
      <iframe
        ref={this.iframeElement}
        src={getIframeUrl(url, this.basePath)}
        title={t('aria:adminIframe')}
      />
    );
  }

  /**
   * @param {Event} event
   * @param {Node} event.target
   * @param {string} event.type
   */
  handleEvent({ data, target, type }) {
    const method = `on${type}`;

    if (typeof this[method] == 'function') {
      if (data) {
        this[method](data);
      } else {
        this[method](target);
      }
    }
  }

  /**
   * Handle events that must be managed manually in the
   * mount lifecycle methods in a single location.
   *
   * @param {string} action
   *   `add` or `remove`
   */
  manageNativeEvents(action) {
    const { current: iframe } = this.iframeElement;

    iframe[`${action}EventListener`]('load', this);
    window[`${action}EventListener`]('message', this);
  }

  /**
   * Map instance props (action dispatchers) to the
   * post message names sent from the iframe window.
   */
  registerPostMessages() {
    const { onOverlayClose, onOverlayOpen } = this.props;

    this.postMessages = {
      'IFRAME:OVERLAY:OPEN': onOverlayOpen,
      'IFRAME:OVERLAY:CLOSE': onOverlayClose,
    };
  }

  /**
   * The `iframe` **element**'s load event listener does always
   *
   * - intercept the login page in the framed window
   * - inject an override style sheet
   * - attach a mutation observer to the iframe DOM for modal dialogs
   * - add an `unload` event listener to the current iframe window
   *
   * **If** navigation was initiated in the iframe, it dispatches a route action.
   * That updates the store but cancels this component's `render` in
   * `shouldComponentUpdate` when the next store state is passed with props.
   *
   * @param {Window} contentWindow
   */
  onload({ contentWindow }) {
    if (this.abortLoadHandler(contentWindow)) {
      return;
    }

    const { onUnload } = this.props;

    contentWindow.addEventListener('unload', onUnload);
    this.setStyleOverride(contentWindow);
    this.syncParentState(contentWindow);
  }

  /**
   * Handle iframe action side effects, if any.
   *
   * @param {Window} iframeWindow
   */
  syncParentState(iframeWindow) {
    const { hasOverlay, onOverlayClose, route } = this.props;

    // Iframe navigation can happen without an open overlay
    // being closed, e.g. "GET after POST".
    if (hasOverlay) {
      onOverlayClose();
    }

    const iframeUrl = getUrl(iframeWindow);
    const parentUrl = getParentUrl(iframeUrl, this.basePath);

    if (this.isNavigationInitiatedInIframe(parentUrl)) {
      route({
        path: parentUrl,
      });
    }
  }

  /**
   * @param {*} messageData
   */
  onmessage(messageData) {
    if (this.postMessages.hasOwnProperty(messageData)) {
      this.postMessages[messageData]();
    }
  }

  /**
   * @param {Window} iframeWindow
   * @return {boolean}
   */
  isSameOrigin(iframeWindow) {
    return iframeWindow.location.origin === window.location.origin;
  }

  /**
   * @param {string} pathComponent
   * @return {boolean}
   */
  isInternPathComponent(pathComponent) {
    const internExpression = /^\/intern(?:$|\/)/;

    return internExpression.test(pathComponent);
  }

  /**
   * @param {Window} iframeWindow
   * @return {boolean}
   */
  isInternWindow(iframeWindow) {
    if (this.isSameOrigin(iframeWindow)) {
      const { pathname } = iframeWindow.location;

      return this.isInternPathComponent(pathname);
    }

    return false;
  }

  /**
   * @param {Window} iframeWindow
   * @return {boolean}
   */
  abortLoadHandler(iframeWindow) {
    if (iframeWindow.location.pathname === LOGIN_PATH) {
      login(getUrl());

      return true;
    }

    if (this.isInternWindow(iframeWindow)) {
      navigate(iframeWindow.location.pathname);

      return true;
    }

    return false;
  }

  /**
   * @param {string} expectedUrl
   *   The app URL that was resolved from
   *   the iframe window's location object.
   * @return {boolean}
   */
  isNavigationInitiatedInIframe(expectedUrl) {
    const currentUrl = getUrl();

    return currentUrl !== expectedUrl;
  }

  /**
   * Dynamically insert the Style Sheet with app specific overrides.
   * Modifying the sources in `zaaksysteem` can cause regressions
   * in `PIP` and `/form`.
   *
   * @param {Window} contentWindow
   */
  setStyleOverride(contentWindow) {
    const link = contentWindow.document.createElement('link');

    link.onload = this.props.onLoad;
    link.rel = 'stylesheet';
    link.href = '/admin/legacy.css';
    contentWindow.document.head.appendChild(link);
  }

  /**
   * Get the app's base path that is configured in nginx.
   * That value should always be resolved programmatically.
   *
   * @example
   * // https://example.org/foo/bar
   * this.getBasePath(); // => '/foo/'
   *
   * @return {string}
   */
  getBasePath() {
    const { pathname } = window.location;
    const [, basePath] = /^(\/[^/]+\/)/.exec(pathname);

    return basePath;
  }

  /**
   * To determine if the iframe is stale,
   * compare the location data of both the
   * parent window and the iframe window.
   *
   * @param {string} nextUrl
   * @param {string} nextIframeUrl
   * @param {string} currentIframeUrl
   * @return {boolean}
   */
  isIframeStale(nextUrl, nextIframeUrl, currentIframeUrl) {
    const { url } = this.props;
    const isParentStale = nextUrl === url;
    const isIframeStale = nextIframeUrl === currentIframeUrl;

    return isParentStale || isIframeStale;
  }

  /**
   * @param {string} currentIframeUrl
   *   The iframe window's current path and query component.
   * @param {string} nextIframeUrl
   *   The iframe window's next path and query component.
   * @param {string} iframeElementSrc
   *   The value of the iframe element's `src` property.
   *   This does **not** reflect navigation that was subsequently
   *   initiated in the iframe, but the last navigation before that.
   * @return {boolean}
   */
  isIframeOutOfSync(currentIframeUrl, nextIframeUrl, iframeElementSrc) {
    const parsedSrcUrl = parseUrl(iframeElementSrc);
    const isBaseRoute = nextIframeUrl === parsedSrcUrl;

    return isBaseRoute && parsedSrcUrl !== currentIframeUrl;
  }
}
