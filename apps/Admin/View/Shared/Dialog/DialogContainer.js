import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import Dialog from './Dialog';
import { hideDialog } from '../../../Store/UI/ui.actions';
import { invoke } from '../../../Store/Route/route.actions';
import { discard } from '../../../Store/SystemConfiguration/systemconfiguration.actions';

const mapStateToProps = ({ ui: { dialog }, route }) => ({ dialog, route });

const mapDispatchToProps = dispatch => ({
  hide: payload => dispatch(hideDialog(payload)),
  invoke: payload => dispatch(invoke(payload)),
  discard: options => dispatch(discard(options)),
});

const DialogContainer = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Dialog)
);

export default DialogContainer;
