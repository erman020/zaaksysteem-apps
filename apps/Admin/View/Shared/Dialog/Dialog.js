import React from 'react';
import * as Dialogs from '../../SystemConfiguration/Dialogs';
import { get } from '@mintlab/kitchen-sink';

/**
 * Renders a specific dialog, based on the type defined in `ui.dialog.type`
 *
 * @param {Object} props
 * @return {ReactElement}
 */
const DialogWrapper = props => {
  const { dialog } = props;
  const dialogType = get(dialog, 'type');

  if (!dialogType) return null;

  const Dialog = Dialogs[dialogType];

  if (!Dialog) {
    throw new Error(`Dialog component for '${dialogType}' does not exist.`);
  }

  return <Dialog {...props} />;
};

export default DialogWrapper;
