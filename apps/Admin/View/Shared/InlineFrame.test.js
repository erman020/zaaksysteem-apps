import InlineFrame from './InlineFrame';

/**
 * @test {InlineFrame}
 */
describe('The `InlineFrame` component', () => {
  describe('has a `getBasePath` instance method', () => {
    test('that gets the first path component segment including leading and trailing slash', () => {
      window.location.assign('/foo/bar');

      const iframe = new InlineFrame({});
      const actual = iframe.getBasePath();
      const expected = '/foo/';

      expect(actual).toBe(expected);
    });
  });

  describe('has an `isInternPathComponent` method that returns', () => {
    test('`true` if the only path segment is `intern` without a trailing slash', () => {
      const iframe = new InlineFrame({});
      const actual = iframe.isInternPathComponent('/intern');
      const expected = true;

      expect(actual).toBe(expected);
    });

    test('`true` if the only path segment is `intern` with a trailing slash', () => {
      const iframe = new InlineFrame({});
      const actual = iframe.isInternPathComponent('/intern/');
      const expected = true;

      expect(actual).toBe(expected);
    });

    test('`true` if the first of multiple path segments is `intern`', () => {
      const iframe = new InlineFrame({});
      const actual = iframe.isInternPathComponent('/intern/tail');
      const expected = true;

      expect(actual).toBe(expected);
    });

    test('`false` in all other cases', () => {
      const iframe = new InlineFrame({});
      const actual = iframe.isInternPathComponent('/tail');
      const expected = false;

      expect(actual).toBe(expected);
    });
  });
});
