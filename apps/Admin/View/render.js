import React from 'react';
import { render } from 'react-dom';
import AdminApp from './Shared/App/Provider';
import '@mintlab/ui/distribution/ui.css';
import './render.css';

const rootNode = document.getElementById('zs-app');

window.onerror = null;
render(<AdminApp />, rootNode);

if (module.hot) {
  module.hot.accept();
}
