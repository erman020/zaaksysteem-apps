import {
  addRowData,
  getRowById,
  getBreadcrumbs,
  getPathToItem,
  mergeSelectedIntoRows,
} from './CatalogContainer';

const ONE = 1;

describe('The `CatalogContainer` module', () => {
  describe('The `addRowData` function', () => {
    const data = {
      id: '1',
      type: 'folder',
    };

    test('adds a `path` string to the row data', () => {
      const { path } = addRowData(data);
      expect(path).toEqual(getPathToItem(data.id));
    });

    test('adds a `isFolder` boolean to the row data', () => {
      const { isFolder } = addRowData(data);
      expect(isFolder).toBeTruthy();
    });
  });

  describe('The `getRowById` function that', () => {
    test('`returns the row where the id matches the selected id`', () => {
      const rows = [
        {
          type: 'first',
          id: 'unique1',
        },
        {
          type: 'second',
          id: 'unique2',
        },
        {
          type: 'third',
          id: 'unique3',
        },
      ];
      const selected = 'unique2';
      const actual = getRowById(rows, selected);
      const expected = { type: 'second', id: 'unique2' };

      expect(actual).toEqual(expected);
    });
  });

  describe('The `getPathToItem` function', () => {
    const basePath = '/admin/catalogus';
    test('returns a full path when given a `id`', () => {
      expect(getPathToItem('test')).toEqual(`${basePath}/test`);
    });

    test('returns a base path when not given a `id`', () => {
      expect(getPathToItem()).toEqual(basePath);
    });
  });

  describe('The `getBreadcrumbs` function', () => {
    test('filters `null` values', () => {
      const breadcrumbs = {
        parent: null,
        current: {
          name: 'Test',
          id: '1',
        },
      };

      expect(getBreadcrumbs(breadcrumbs)).toHaveLength(ONE);
    });
  });

  describe('The `mergeSelectedIntoRows` function that', () => {
    test('`returns the rows with a boolean Selected when it is`', () => {
      const data = [
        {
          id: 'a',
        },
        {
          id: 'b',
        },
        {
          id: 'c',
        },
      ];
      const selected = ['a', 'c'];
      const actual = mergeSelectedIntoRows(data, selected);
      const expected = [
        {
          id: 'a',
          selected: true,
        },
        {
          id: 'b',
          selected: false,
        },
        {
          id: 'c',
          selected: true,
        },
      ];

      expect(actual).toEqual(expected);
    });
  });
});
