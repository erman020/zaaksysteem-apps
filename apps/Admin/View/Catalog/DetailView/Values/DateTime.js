import React from 'react';
import { valuesStyleSheet } from './Values.style';
import { withStyles } from '@mintlab/ui';
import { withFormatDate } from '../../../Shared/App/withFormatDate';

/**
 * @reactProps {Object} classes
 * @reactProps {Object} value
 * @reactProps {Object} t
 * @return {ReactElement}
 */
const DateTime = ({ classes, value, formatDate }) => {
  const dateValue = new Date(value);

  return (
    <div>
      <span>{formatDate(dateValue, 'Do MMMM YYYY')}</span>
      <span className={classes.time}>{formatDate(dateValue, 'shortTime')}</span>
    </div>
  );
};

export default withStyles(valuesStyleSheet)(withFormatDate(DateTime));
