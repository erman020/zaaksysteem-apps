import React, { createElement } from 'react';
import { detailsStyleSheet } from './Details.style';
import { withStyles, Button, Icon, Render } from '@mintlab/ui';
import { asArray, get } from '@mintlab/kitchen-sink';
import { DateTime, Default } from './Values';

const iconConfig = {
  downloadLink: 'save_alt',
  id: 'code',
  identification: 'fingerprint',
  lastModified: 'access_time',
  magicString: 'photo_filter',
  used_in_case_types: 'ballot',
  used_in_object_types: 'layers',
  valueType: 'view_quilt',
};

const getIcon = name => get(iconConfig, name, 'info');

const translate = (string, translations = {}) => {
  if (translations.hasOwnProperty(string)) {
    return translations[string];
  }

  return string;
};

const valueFormats = {
  lastModified: DateTime,
};

/**
 * @param {string} type
 * @param {string} value
 * @param {Function} t
 * @return {ReactElement}
 */
const formatValue = (type, value, t) => {
  const translations = {
    valueType: t('attributes:types', { returnObjects: true }),
  };

  const El =
    value && valueFormats.hasOwnProperty(type) ? valueFormats[type] : Default;

  return createElement(El, {
    key: value,
    value:
      translate(value, translations[type]) ||
      t('catalog:detailView:valueFallback'),
  });
};

const createValueList = (type, values, t) =>
  asArray(values).map(value => formatValue(type, value, t));

const valueButtons = {
  id: 'open_in_new',
  enclosedDocument: 'save_alt',
};

/**
 * @param {string} link
 */
const openLink = link => {
  window.open(link, '_blank').focus();
};

/**
 * @reactProps {Object} classes
 * @reactProps {Object} detail
 * @reactProps {string} detail.type
 * @reactProps {string} detail.value
 * @reactProps {string} detail.link
 * @reactProps {Function} t
 * @return {ReactElement}
 */
const Detail = ({ classes, detail: { type, value, link }, t }) => (
  <div className={classes.wrapper}>
    <div className={classes.header}>
      <div className={classes.icon}>
        <Icon>{getIcon(type) || 'info'}</Icon>
      </div>
      <div className={classes.title}>
        {translate(type, t('catalog:detailView', { returnObjects: true }))}
      </div>
    </div>
    <div className={classes.content}>
      <div className={classes.values}>{createValueList(type, value, t)}</div>
      <Render condition={link}>
        <Button
          classes={{
            root: classes.valueActionButton,
          }}
          action={() => openLink(link)}
          presets={['icon', 'extraSmall']}
        >
          {valueButtons[type]}
        </Button>
      </Render>
    </div>
  </div>
);

export default withStyles(detailsStyleSheet)(Detail);
