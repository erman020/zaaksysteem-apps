import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { Icon } from '@mintlab/ui';
import { filterProperties, get } from '@mintlab/kitchen-sink';
import Catalog from './Catalog';
import {
  fetchCatalog,
  toggleCatalogItem,
} from '../../Store/Catalog/Items/items.actions';
import { fetchCatalogItem } from '../../Store/Catalog/Details/details.actions';
import { AJAX_STATE_PENDING } from '../../../library/redux/ajax/createAjaxConstants';
import { invoke } from '../../Store/Route/route.actions';

/**
 * Get path to catalog item
 * @param {string} id
 * @return {string}
 */
export const getPathToItem = id => {
  const basePath = '/admin/catalogus';
  return id ? `${basePath}/${id}` : basePath;
};

/**
 * @param {Object} row
 * @return {Array}
 */
export const addRowData = row => ({
  ...row,
  path: getPathToItem(row.id),
  isFolder: row.type === 'folder',
  icon: row.type,
  id: row.id,
});

export const markSelected = selected => row => ({
  ...row,
  selected: selected.includes(row.id),
});

/**
 * @param {Array} rows
 * @param {Array} selected
 * @return {Array}
 */
export const mergeSelectedIntoRows = (rows, selected) =>
  rows.map(row => ({
    ...row,
    ...{
      selected: selected.includes(row.id),
    },
  }));

/**
 * @param {Array} rows
 * @param {Array} selected
 * @return {*}
 */
export const getRowById = (rows, selected) =>
  rows.find(row => row.id === selected);

/**
 * Transform parent and self into breadcrumbs
 * @param {Object} breadcrumbs
 * @return {Array<Object>}
 */
export const getBreadcrumbs = breadcrumbs => {
  const grandparent = get(breadcrumbs, 'grandparent', null);
  const parent = get(breadcrumbs, 'parent', null);
  const current = get(breadcrumbs, 'current', null);
  const breadcrumbsInOrder = [grandparent, parent, current];

  return breadcrumbsInOrder
    .filter(item => item && item.id)
    .map(({ id, name }) => ({
      path: getPathToItem(id),
      label: name,
    }));
};

/**
 * @param {Object} state
 * @param {Object} state.catalog
 * @param {Object} state.catalog.items
 * @param {Object} state.catalog.details
 * @return {Object}
 */
const mapStateToProps = ({ catalog: { items, details } }) => ({
  breadcrumbs: getBreadcrumbs(items.breadcrumbs),
  catalogItem: details.data,
  currentFolderName: items.currentFolderName,
  detailView: items.detailView,
  items: items.items,
  loadingDetailView: details.state === AJAX_STATE_PENDING,
  loadingTable: items.state === AJAX_STATE_PENDING,
  selectedItems: items.selectedItems,
  showDetailView: details.showDetailView,
});

/**
 * @param {Function} dispatch
 * @param {Function} props
 * @param {Array} props.segments
 * @return {Object}
 */
const mapDispatchToProps = (dispatch, { segments }) => {
  const [id] = segments;

  const dispatchInvoke = payload => dispatch(invoke(payload));
  const doNavigate = path => {
    dispatchInvoke({
      path,
    });
  };
  const onRowNavigate = ({ isFolder, path }) => {
    if (isFolder) {
      doNavigate(path);
    }
  };

  return {
    doNavigate,
    fetchCatalogItem: () => dispatch(fetchCatalogItem()),
    fetchNewData: () => dispatch(fetchCatalog(id)),
    invoke: dispatchInvoke,
    onRowNavigate,
    toggleItem: bindActionCreators(toggleCatalogItem, dispatch),
  };
};

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    selectedItems,
    breadcrumbs,
    showDetailView,
    items,
    catalogItem,
    currentFolderName,
  } = stateProps;
  const { t } = ownProps;
  const title = t('catalog:title');

  /**
   * @param {string} name
   * @return {Object}
   */
  const mapColumns = name => ({
    name,
    label: t(`catalog:column:${name}`),
  });

  const columns = ['icon', 'name', 'type'].map(mapColumns);

  const rows = items.map(addRowData);
  const selectedRow = getRowById(rows, selectedItems[0]) || {};
  const selectedCount = selectedItems.length;

  const detailInfoConfig = {
    2: {
      name: `${selectedCount} ${t('catalog:detailView:itemsSelected')}`,
      icon: 'select_all',
    },
    1: {
      ...filterProperties(selectedRow, 'name', 'icon'),
      ...filterProperties(
        catalogItem,
        'version',
        'type',
        'details',
        'relations'
      ),
    },
    0: {
      name: currentFolderName || title,
      icon: 'folder',
    },
  };

  const detailInfo = detailInfoConfig[Math.min(selectedCount, 2)];

  const breadcrumbsWithRoot = [
    {
      path: getPathToItem(),
      label: title,
    },
    ...breadcrumbs,
  ];

  const speedDialActions = [
    {
      icon: <Icon>publish</Icon>,
      title: t('catalog:actions:import'),
      action() {
        window.location.href =
          '/beheer/object/import/1?return_url=/admin/catalogus/{}';
      },
    },
  ];

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    speedDialActions,
    columns,
    breadcrumbs: breadcrumbsWithRoot,
    detailInfo,
    rows: mergeSelectedIntoRows(rows, selectedItems),
    selectedRow,
    selectedCount,
    showDetailView,
    t,
  };
};

/**
 * Connects {@link Catalog} with {@link i18next}, and
 * the store.
 * @return {ReactElement}
 */
const CatalogContainer = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(Catalog)
);

export default CatalogContainer;
