import React from 'react';
import classNames from 'classnames';
import deepmerge from 'deepmerge';
import { catalogStyleSheet } from './Catalog.style';
import { sharedStylesheet } from '../Shared/Shared.style';
import CatalogHeader from './CatalogHeader/CatalogHeader';
import CatalogTable from './table/CatalogTable';
import DetailView from './DetailView/DetailView';
import { withStyles, Card, Loader, Render, SpeedDial } from '@mintlab/ui';

/**
 * @reactProps {Object} classes
 * @reactProps {Array<Object>} breadcrumbs
 * @reactProps {Array} columns
 * @reactProps {Object} detailInfo
 * @reactProps {Function} doNavigate
 * @reactProps {boolean} loadingTable
 * @reactProps {boolean} loadingDetailView
 * @reactProps {Function} onRowNavigate
 * @reactProps {Array<Object>} rows
 * @reactProps {boolean} showDetailView
 * @reactProps {Function} t
 * @reactProps {Function} toggleDetailView
 * @return {ReactElement}
 */
const Catalog = ({
  classes,
  breadcrumbs,
  speedDialActions,
  columns,
  detailInfo,
  doNavigate,
  toggleItem,
  loadingTable,
  loadingDetailView,
  onRowNavigate,
  rows,
  showDetailView,
  t,
  toggleDetailView,
}) => (
  <div className={classes.wrapper}>
    <CatalogHeader breadcrumbs={breadcrumbs} doNavigate={doNavigate} />
    <div className={classes.contentWrapper}>
      <SpeedDial
        actions={speedDialActions}
        direction="up"
        hidden={false}
        scope="catalog-actions"
      />
      <div
        className={classNames(classes.sheet, {
          [classes.sheetWidth]: showDetailView,
        })}
      >
        <Card className={classes.tableWrapper}>
          <CatalogTable
            columns={columns}
            rows={rows}
            onRowNavigate={onRowNavigate}
            toggleItem={toggleItem}
            t={t}
          />
          {loadingTable && <Loader className={classes.loader} />}
        </Card>
      </div>
      <Render condition={showDetailView}>
        <DetailView
          closeDetailView={toggleDetailView}
          detailInfo={detailInfo}
          loading={loadingDetailView}
          t={t}
        />
      </Render>
    </div>
  </div>
);

/**
 * @param {Object} theme
 * @return {JSS}
 */
const mergedStyles = theme =>
  deepmerge(sharedStylesheet(theme), catalogStyleSheet(theme));

export default withStyles(mergedStyles)(Catalog);
