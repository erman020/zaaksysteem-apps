import React from 'react';
import deepmerge from 'deepmerge';
import { TableRow, TableCell, withStyles } from '@mintlab/ui';
import { LinkCell, IconCell } from '../../Shared/Table/cells';
import { catalogStyleSheet } from '../Catalog.style';
import { sharedStylesheet } from '../../Shared/Shared.style';
import { preventDefaultAndCall } from './../../../../library/preventDefaultAndCall';

/**
 * @reactProps {string} icon
 * @reactProps {string} path
 * @reactProps {string} type
 * @reactProps {string} name
 * @reactProps {boolean} selected
 * @reactProps {boolean} isFolder
 * @reactProps {Object} classes
 * @reactProps {Function} onRowNavigate
 *
 * @returns {ReactElement}
 */
class CatalogTableRow extends React.Component {
  shouldComponentUpdate(nextProps) {
    return (
      nextProps.selected !== this.props.selected ||
      nextProps.id !== this.props.id
    );
  }

  render() {
    const {
      icon,
      path,
      type,
      name,
      selected,
      isFolder,
      onRowNavigate,
      classes,
      t,
      ...rest
    } = this.props;

    const handleNavigate = preventDefaultAndCall(() =>
      onRowNavigate({
        path,
        isFolder,
      })
    );

    const scope = `catalog-item:${type}`;

    return (
      <TableRow
        checkboxes={true}
        pointer={true}
        selected={selected}
        onDoubleClick={handleNavigate}
        scope={scope}
        {...rest}
      >
        <TableCell className={classes.iconCell}>
          <IconCell value={icon} />
        </TableCell>
        <TableCell>
          <LinkCell
            handleNavigate={handleNavigate}
            path={path}
            value={name}
            classes={{
              link: classes.linkCell,
            }}
            scope={scope}
          />
        </TableCell>
        <TableCell className={classes.typeCell}>
          {t(`catalog:type:${type}`)}
        </TableCell>
      </TableRow>
    );
  }
}

const mergedStyles = theme =>
  deepmerge(sharedStylesheet(theme), catalogStyleSheet(theme));

export default withStyles(mergedStyles)(CatalogTableRow);
