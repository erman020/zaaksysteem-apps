import React from 'react';
import { Table, TableHead, TableBody, TableRow, TableCell } from '@mintlab/ui';
import CatalogTableRow from './CatalogTableRow';

/**
 * @reactProps {Array<Object>} columns
 * @reactProps {Array<Object>} rows
 * @reactProps {Function} onRowNavigate
 *
 * @returns {ReactElement}
 */
const CatalogTable = ({ columns, rows, onRowNavigate, toggleItem, t }) => (
  <Table>
    <TableHead>
      <TableRow checkboxes={true} heading={true}>
        {columns.map(({ name, label }) => (
          <TableCell key={name} heading={true}>
            {label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
    <TableBody>
      {rows.map((row, index) => (
        <CatalogTableRow
          key={index}
          onRowNavigate={onRowNavigate}
          onClick={event => {
            if (event.target.tagName.toLowerCase() !== 'a') {
              event.stopPropagation();
              toggleItem(row.id);
            }
          }}
          onCheckboxClick={event => {
            event.stopPropagation();
            toggleItem(row.id, true);
          }}
          t={t}
          {...row}
        />
      ))}
    </TableBody>
  </Table>
);

export default CatalogTable;
