/**
 * @param {Object} theme
 * @return {JSS}
 */
export const buttonBarStyleSheet = ({ mintlab: { greyscale } }) => ({
  wrapper: {
    display: 'flex',
    '&>*:not(:last-child):after': {
      content: '""',
      width: '1px',
      height: '30px',
      background: greyscale.darker,
      position: 'absolute',
      bottom: '20px',
    },
  },
});
