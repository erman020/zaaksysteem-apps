import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import ButtonBar from './ButtonBar';
import { toggleCatalogDetailView } from '../../../Store/Catalog/Details/details.actions';
import { catalogExportCasetype } from '../../../Store/Catalog/ButtonBar/buttonBar.actions';

/**
 * @param {Object} state
 * @param {Object} state.catalog
 * @param {Object} state.catalog.details
 * @return {Object}
 */
const mapStateToProps = ({
  catalog: {
    items: { items, selectedItems },
    details,
  },
}) => {
  const selectedCount = selectedItems.length;
  let selectedType = 'none';

  if (selectedCount >= 2) {
    selectedType = 'multiple';
  } else if (selectedCount === 1) {
    const selectedItem = items.find(item => item.id === selectedItems[0]);

    selectedType = selectedItem.type;
  }

  return {
    selectedType,
    showDetailView: details.showDetailView,
  };
};

const mapDispatchToProps = dispatch => ({
  exportCaseType: bindActionCreators(catalogExportCasetype, dispatch),
  toggleDetailView: bindActionCreators(toggleCatalogDetailView, dispatch),
});

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { showDetailView, selectedType } = stateProps;
  const { toggleDetailView, exportCaseType } = dispatchProps;
  const actionButtons = [
    {
      type: 'save_alt',
      action: () => exportCaseType(),
      condition: selectedType === 'case_type',
    },
  ].filter(button => button.condition);
  const permanentButtons = [
    {
      type: 'info',
      action: () => toggleDetailView(),
      active: showDetailView,
    },
  ];
  const buttonSegments = [actionButtons, permanentButtons].filter(
    segment => segment.length
  );

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    buttonSegments,
  };
};

/**
 * Connects {@link ButtonBar} with {@link i18next}, and
 * the store.
 * @return {ReactElement}
 */
const ButtonBarContainer = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(ButtonBar)
);

export default ButtonBarContainer;
