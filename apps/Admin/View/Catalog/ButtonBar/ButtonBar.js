import React from 'react';
import { buttonBarStyleSheet } from './ButtonBar.style';
import { withStyles, Button } from '@mintlab/ui';

/**
 * @reactProps {Array} buttonSegments
 * @return {ReactElement}
 */
const ButtonBar = ({ buttonSegments, classes }) => (
  <div className={classes.wrapper}>
    {buttonSegments.map((buttons, index) => (
      <div key={index}>
        {buttons.map(({ type, action, active }) => (
          <Button
            key={type}
            action={action}
            presets={['icon', ...(active ? ['primary'] : [])]}
            scope={`catalog-header:button-bar:${type}`}
          >
            {type}
          </Button>
        ))}
      </div>
    ))}
  </div>
);

export default withStyles(buttonBarStyleSheet)(ButtonBar);
