import React from 'react';
import { catalogHeaderStyleSheet } from './CatalogHeader.style';
import SubAppHeader from '../../Shared/Header/SubAppHeader';
import ButtonBar from './../ButtonBar/ButtonBarContainer';
import { preventDefaultAndCall } from '../../../../library/preventDefaultAndCall';
import { withStyles, Breadcrumbs } from '@mintlab/ui';

/**
 * @reactProps {Object} classes
 * @reactProps {Array<Object>} breadcrumbs
 * @reactProps {Function} doNavigate
 * @return {ReactElement}
 */
const CatalogHeader = ({ classes, breadcrumbs, doNavigate }) => (
  <SubAppHeader>
    <div className={classes.headerWrapper}>
      <Breadcrumbs
        maxItems={4}
        items={breadcrumbs}
        onItemClick={preventDefaultAndCall(event => {
          doNavigate(event.currentTarget.getAttribute('href'));
        })}
        scope="catalog"
      />
      <ButtonBar />
    </div>
  </SubAppHeader>
);

export default withStyles(catalogHeaderStyleSheet)(CatalogHeader);
