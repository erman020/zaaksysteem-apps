/**
 * @param {Object} theme
 * @return {JSS}
 */
export const catalogHeaderStyleSheet = () => ({
  headerWrapper: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    '&>*:nth-child(1)': {
      display: 'flex',
      alignItems: 'center',
      flexGrow: '1',
    },
    '&>*:not(:first-child):not(:last-child)': {
      marginRight: '10px',
    },
  },
});
