import log from './../Log/log.svg';

const tableMaxWidth = '1600px';
const titleBarHeight = '72px';
const tableMargin = '20px';
const cardPadding = '8px';
const tableSideSpace = `${tableMargin} + ${cardPadding}`;

/**
 * @return {JSS}
 */
export const catalogStyleSheet = ({
  palette: {
    common: { black },
  },
}) => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
  contentWrapper: {
    position: 'relative',
    height: `calc(100% - ${titleBarHeight})`,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  sheet: {
    height: '100%',
    width: '100%',
    margin: 'auto',
    'background-image': `url(${log})`,
    'background-size': '18px',
  },
  sheetWidth: {
    width: 'calc(100% - 500px)',
  },
  tableWrapper: {
    display: 'block',
    maxWidth: tableMaxWidth,
    overflowX: 'auto',
    width: `calc(100% - (${tableSideSpace}) * 2)`,
    minHeight: `calc(100% - ${tableMargin})`,
    margin: `${tableMargin} auto 0 auto`,
    padding: '0px',
  },
  typeCell: {
    width: '150px',
  },
  iconCell: {
    width: '50px',
  },
  linkCell: {
    textDecoration: 'none',
    color: black,
    '&:hover': {
      textDecoration: 'underline',
    },
  },
  loader: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
});
