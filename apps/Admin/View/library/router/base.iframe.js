import Configuration from '../../SystemConfiguration/SystemConfigurationContainer';
import Log from '../../Log/LogContainer';

/**
 * iFrame base segment dictionary.
 *
 * @type {Object}
 */
const dictionary = {
  // Main navigation.
  catalogus: '/beheer/bibliotheek',
  gebruikers: '/medewerker',
  logboek: Log,
  transacties: '/beheer/sysin/transactions',
  koppelingen: '/beheer/sysin/overview',
  gegevens: '/beheer/object/datastore',
  configuratie: Configuration,

  // Orphans. The URLs do *not* reflect their
  // position in the user interface hierarchy.
  import: '/beheer/import',
  objecttypen: '/beheer/objecttypen',
  object: '/beheer/object/import',
  woz: '/beheer/woz',
  zaaktypen: '/beheer/zaaktypen',
};

export default dictionary;
