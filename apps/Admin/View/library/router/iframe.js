import dictionary from './base';
import { getSegment } from '@mintlab/kitchen-sink';

const { keys } = Object;
const ZERO = 0;
const join = (...rest) => rest.join('');

function expandPath(inputBasePath, actualInputPath, outputBasePath) {
  const expression = new RegExp(`^${inputBasePath}([/?].*)?`);
  const [, tail] = expression.exec(actualInputPath);

  if (tail) {
    return join(outputBasePath, tail);
  }

  return outputBasePath;
}

/**
 * If the dictionary value for a given key is a string,
 * it is handled with the InlineFrame component.
 * Otherwise it is an original React component.
 *
 * @param {string} key
 * @return {boolean}
 */
const isIframe = key => typeof dictionary[key] === 'string';

/**
 * Resolve the parent window path component from the iframe path component.
 *
 * @param {string} iframePath
 * @param {string} root
 * @return {string}
 */
export function getParentUrl(iframePath, root) {
  const segment = keys(dictionary)
    .filter(isIframe)
    .find(key => {
      const basePath = dictionary[key];
      const { length } = basePath.split('/');
      const [pathComponent] = iframePath.split('?');
      const iframeBasePath = pathComponent
        .split('/')
        .slice(ZERO, length)
        .join('/');

      return basePath === iframeBasePath;
    });
  const iframeBasePath = dictionary[segment];

  return expandPath(iframeBasePath, iframePath, join(root, segment));
}

/**
 * Resolve the iframe path component from the parent window path component.
 *
 * @param {string} parentPath
 * @param {string} root
 * @return {string}
 */
export function getIframeUrl(parentPath, root) {
  const [pathComponent] = parentPath.split('?');
  const segment = getSegment(pathComponent);
  const iframePath = dictionary[segment];

  return expandPath(join(root, segment), parentPath, iframePath);
}
