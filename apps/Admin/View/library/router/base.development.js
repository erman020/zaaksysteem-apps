/*
 * Development configuration for base segment routing, extends stable.
 */

import Catalog from './../../Catalog/CatalogContainer';
import stable from './base.iframe';

const { assign } = Object;

const development = {
  catalogus: Catalog,
};

export default assign(stable, development);
