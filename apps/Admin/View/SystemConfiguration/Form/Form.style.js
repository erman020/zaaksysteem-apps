import config from './config.svg';

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const formStylesheet = ({ breakpoints }) => ({
  sheet: {
    flex: 1,
    padding: '30px',
    'background-image': `url(${config})`,
    'background-size': '18px',
  },
  form: {
    width: '100%',
    [breakpoints.up('lg')]: {
      maxWidth: breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  formRow: {
    display: 'flex',
    'flex-wrap': 'wrap',
    '& + $formRow': {
      marginTop: '28px',
    },
  },
  colLeft: {
    flex: 1,
    marginRight: '20px',
    [breakpoints.up('lg')]: {
      flex: 'none',
      width: '50%',
    },
  },
  colRight: {
    width: '30px',
    'align-self': 'center',
  },
  label: {
    flex: '0 0 100%',
    marginBottom: '6px',
  },
});
