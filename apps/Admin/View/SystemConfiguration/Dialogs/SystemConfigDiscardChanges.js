import React from 'react';
import { Dialog } from '@mintlab/ui';
import { get } from '@mintlab/kitchen-sink';

const confirm = ({ dialog, discard }) => {
  const options = get(dialog, 'options');
  discard({
    ...options,
  });
};

/**
 * Discard Changes {@link DialogWrapper}.
 *
 * @param {Object} props
 * @param {Object} props.ui
 * @param {Function} props.t
 * @param {*} props.hide
 * @param {*} props.invoke
 * @return {ReactElement}
 */
const SystemConfigDiscardChanges = ({ dialog, t, hide, discard }) => (
  <Dialog
    open={true}
    title={t('dialog:discardChanges:title')}
    text={t('dialog:discardChanges:text')}
    buttons={[
      {
        text: t('dialog:ok'),
        onClick: () =>
          confirm({
            dialog,
            discard,
          }),
        presets: ['primary', 'raised'],
      },
      {
        text: t('dialog:cancel'),
        onClick: () => hide(),
        presets: ['secondary', 'raised'],
      },
    ]}
  />
);

export default SystemConfigDiscardChanges;
