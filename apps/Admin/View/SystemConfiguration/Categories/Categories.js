import React from 'react';
import { VerticalMenu } from '@mintlab/ui';
import { withStyles } from '@mintlab/ui';
import { categoriesStyleSheet } from './Categories.style';

const { assign } = Object;

/**
 * @param {Object} props
 * @param {Array} props.categories
 * @param {Function} props.invoke
 * @param {Object} props.classes
 * @return {ReactElement}
 */
const Categories = ({ categories, invoke, classes }) => {
  const routeTo = slug => {
    invoke({
      path: `/admin/configuratie/${slug}`,
    });
  };

  const mapCategories = category =>
    assign({}, category, {
      action: routeTo.bind(this, category.slug),
      active: category.current,
    });

  return (
    <VerticalMenu
      items={categories.map(mapCategories)}
      classes={{
        menu: classes.menu,
      }}
    />
  );
};

export default withStyles(categoriesStyleSheet)(Categories);
