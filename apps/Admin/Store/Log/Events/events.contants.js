import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const EVENTS_FETCH = createAjaxConstants('LOG:EVENTS_FETCH');
export const EVENTS_SET_ROWS_PER_PAGE = 'LOG:EVENTS_SET_ROWS_PER_PAGE';
export const EVENTS_SET_PAGE = 'LOG:EVENTS_SET_PAGE';
export const EVENTS_EXPORT = 'LOG:EVENTS_EXPORT';
