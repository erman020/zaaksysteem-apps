import { get } from '@mintlab/kitchen-sink';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import {
  EVENTS_FETCH,
  EVENTS_SET_ROWS_PER_PAGE,
  EVENTS_SET_PAGE,
} from './events.contants';
import {
  isCompleteLogPath,
  getPageAndRowsFromPath,
} from '../library/log.functions';

export const defaultInitialState = {
  data: [],
  count: 0,
  page: 1,
  rowsPerPage: 50,
  state: AJAX_STATE_INIT,
};

const getInitialStateFromUrl = defaultState => {
  const { page, rowsPerPage } = getPageAndRowsFromPath(
    window.location.pathname
  );

  return {
    ...defaultState,
    page,
    rowsPerPage,
  };
};

const getInitialState = () => {
  const { pathname } = window.location;

  return isCompleteLogPath(pathname)
    ? getInitialStateFromUrl(defaultInitialState)
    : defaultInitialState;
};

const ajaxStateChangeReducer = handleAjaxStateChange(EVENTS_FETCH);

export const eventsFetchSuccessReducer = (state, action) => {
  const { response, page, rowsPerPage } = action.payload;

  const instance = get(response, 'result.instance');
  const count = get(instance, 'pager.total_rows');
  const rows = get(instance, 'rows').map(row => ({
    caseId: get(row, 'instance.case_id'),
    date: get(row, 'instance.date_created'),
    description: get(row, 'instance.event_data.human_readable'),
    component: get(row, 'instance.component'),
    user: {
      displayName: get(
        row,
        'instance.created_by.instance.subject.instance.display_name'
      ),
    },
  }));

  return {
    ...state,
    count,
    page: parseInt(page, 10),
    rowsPerPage: parseInt(rowsPerPage, 10),
    data: rows,
  };
};

export const setRowsPerPageReducer = (state, { payload }) => {
  const newPage = Math.ceil(
    (state.page * Math.min(state.count, state.rowsPerPage)) /
      payload.rowsPerPage
  );

  return {
    ...state,
    rowsPerPage: payload.rowsPerPage,
    page: newPage,
  };
};

export const setPageReducer = (state, { payload }) => ({
  ...state,
  page: payload.page,
});

export function events(state = getInitialState(), action) {
  switch (action.type) {
    case EVENTS_FETCH.PENDING:
    case EVENTS_FETCH.ERROR:
      return ajaxStateChangeReducer(state, action);

    case EVENTS_FETCH.SUCCESS:
      return eventsFetchSuccessReducer(
        ajaxStateChangeReducer(state, action),
        action
      );

    case EVENTS_SET_ROWS_PER_PAGE:
      return setRowsPerPageReducer(state, action);

    case EVENTS_SET_PAGE:
      return setPageReducer(state, action);

    default:
      return state;
  }
}
