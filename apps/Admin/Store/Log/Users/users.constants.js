import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const USERS_FETCH_LIST = createAjaxConstants('USERS_FETCH_LIST');
