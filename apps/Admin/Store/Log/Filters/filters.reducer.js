import { get } from '@mintlab/kitchen-sink';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import { FILTERS_APPLY, FILTERS_FETCH_USER_LABEL } from './filters.constants';
import { pathIsInLog, getFiltersFromPath } from '../library/log.functions';

const getInitialStateFromUrl = defaultState => {
  const { keyword, caseNumber, user } = getFiltersFromPath(
    window.location.search
  );

  return {
    caseNumber,
    keyword,
    user: {
      ...defaultState.user,
      ...user,
    },
  };
};

const getInitialState = () => {
  const defaultState = {
    caseNumber: '',
    keyword: '',
    user: {
      state: AJAX_STATE_INIT,
      uuid: '',
      label: '',
    },
  };

  return pathIsInLog(window.location.pathname)
    ? getInitialStateFromUrl(defaultState)
    : defaultState;
};

const fetchUserLabelSuccess = (userState, action) => {
  const result = get(action, 'payload.response.result');
  const uuid = get(result, 'reference');
  const label = get(result, 'instance.display_name');

  return {
    ...userState,
    uuid,
    label,
  };
};

const filtersApply = (state, action) => {
  const { caseNumber, keyword, user } = action.payload;

  const newUser = user
    ? {
        ...state.user,
        label: user.label,
        uuid: user.uuid,
      }
    : {
        ...state.user,
        uuid: '',
        label: '',
      };

  return {
    caseNumber: caseNumber || '',
    keyword: keyword || '',
    user: newUser,
  };
};

const handleAjaxState = handleAjaxStateChange(FILTERS_FETCH_USER_LABEL);

export function filters(state = getInitialState(), action) {
  switch (action.type) {
    case FILTERS_APPLY:
      return filtersApply(state, action);

    case FILTERS_FETCH_USER_LABEL.PENDING:
    case FILTERS_FETCH_USER_LABEL.ERROR:
      return {
        ...state,
        user: handleAjaxState(state.user, action),
      };

    case FILTERS_FETCH_USER_LABEL.SUCCESS:
      return {
        ...state,
        user: fetchUserLabelSuccess(
          handleAjaxState(state.user, action),
          action
        ),
      };

    default:
      return state;
  }
}
