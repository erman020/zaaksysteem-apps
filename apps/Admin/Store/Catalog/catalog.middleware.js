import { catalogClearSelected } from './Items/items.actions';
import { fetchCatalog } from './Items/items.actions';
import { fetchCatalogItem } from './Details/details.actions';
import { CATALOG_TOGGLE_DETAIL_VIEW } from './Details/details.constants';
import { CATALOG_TOGGLE_ITEM } from './Items/items.constants';
import { CATALOG_EXPORT_CASETYPE } from './ButtonBar/buttonBar.constants';
import { navigate } from './../../../library/url';
import { buildUrl, get } from '@mintlab/kitchen-sink';

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const handleRouteInvoke = (store, next, action) => {
  const { catalog } = store.getState();
  const { path } = action.payload;
  const [, , location, id] = path.split('/');

  if (catalog.items.selectedItems.length > 0) {
    store.dispatch(catalogClearSelected());
  }

  if (location === 'catalogus') {
    store.dispatch(fetchCatalog(id));
  }

  return next(action);
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
export const handleDetailView = (store, next, action) => {
  next(action);

  const { catalog } = store.getState();
  const {
    details: { showDetailView, data },
    items: { items, selectedItems },
  } = catalog;

  const selectedItemId = selectedItems[0];
  const selectedItem = items.find(item => item.id === selectedItemId);

  if (
    showDetailView &&
    selectedItems.length === 1 &&
    selectedItemId !== data.id
  ) {
    store.dispatch(fetchCatalogItem(selectedItem.type, selectedItemId));
  }
};

export const handleExportCaseType = (store, next, action) => {
  const { catalog } = store.getState();
  const selectedItemId = get(catalog, 'items.selectedItems[0]');

  const url = buildUrl(`/beheer/zaaktypen/${selectedItemId}/export`, {});

  navigate(url);

  return next(action);
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const catalogMiddleware = store => next => action => {
  const { type } = action;

  switch (type) {
    case 'ROUTE:INVOKE':
      return handleRouteInvoke(store, next, action);

    case CATALOG_TOGGLE_DETAIL_VIEW:
    case CATALOG_TOGGLE_ITEM:
      return handleDetailView(store, next, action);

    case CATALOG_EXPORT_CASETYPE:
      return handleExportCaseType(store, next, action);

    default:
      return next(action);
  }
};

export default catalogMiddleware;
