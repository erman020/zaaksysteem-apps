import { buildUrl } from '@mintlab/kitchen-sink';
import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import {
  CATALOG_FETCH,
  CATALOG_TOGGLE_ITEM,
  CATALOG_CLEAR_SELECTED,
} from './items.constants';

const fetchAjaxAction = createAjaxAction(CATALOG_FETCH);

export const fetchCatalog = id =>
  fetchAjaxAction({
    url: buildUrl('/api/v2/admin/catalog/get_folder_contents', {
      folder_id: id,
    }),
    method: 'GET',
  });

export const toggleCatalogItem = (id, multiSelect = false) => ({
  type: CATALOG_TOGGLE_ITEM,
  payload: {
    id,
    multiSelect,
  },
});

export const catalogClearSelected = () => ({
  type: CATALOG_CLEAR_SELECTED,
});
