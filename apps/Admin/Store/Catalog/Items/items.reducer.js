import { get } from '@mintlab/kitchen-sink';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_FETCH,
  CATALOG_TOGGLE_ITEM,
  CATALOG_CLEAR_SELECTED,
} from './items.constants';

const getBreadcrumbItem = (response, path) => {
  const link = get(response, path);

  return link === null
    ? null
    : {
        name: link.name,
        id: link.id,
      };
};

export const fetchCatalogSuccess = (state, action) => {
  const { response } = action.payload;

  return {
    ...state,
    currentFolderName: get(response, 'links.self.name'),
    items: response.data.map(element => ({
      type: element.attributes.type,
      name: element.attributes.name,
      id: element.id,
    })),
    breadcrumbs: {
      current: getBreadcrumbItem(response, 'links.self'),
      parent: getBreadcrumbItem(response, 'links.parent'),
      grandparent: getBreadcrumbItem(response, 'links.grandparent'),
    },
  };
};

export const selectItem = (state, id, multiSelect = false) => ({
  ...state,
  selectedItems: [...(multiSelect ? state.selectedItems : []), id],
});

export const deselectItem = (state, id) => ({
  ...state,
  selectedItems: state.selectedItems.filter(item => item !== id),
});

export const toggleSelectedItem = (state, action) => {
  const { id, multiSelect } = action.payload;
  const shouldDeselectItem = state.selectedItems.includes(id) && multiSelect;

  return shouldDeselectItem
    ? deselectItem(state, id)
    : selectItem(state, id, multiSelect);
};

export const initialState = {
  state: AJAX_STATE_INIT,
  selectedItems: [],
  items: [],
};

export function items(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(CATALOG_FETCH);

  switch (action.type) {
    case CATALOG_FETCH.PENDING:
    case CATALOG_FETCH.ERROR:
      return handleAjaxState(state, action);

    case CATALOG_FETCH.SUCCESS:
      return fetchCatalogSuccess(handleAjaxState(state, action), action);

    case CATALOG_TOGGLE_ITEM:
      return toggleSelectedItem(state, action);

    case CATALOG_CLEAR_SELECTED:
      return {
        ...state,
        selectedItems: [],
      };

    default:
      return state;
  }
}

export default items;
