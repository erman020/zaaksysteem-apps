import { CATALOG_EXPORT_CASETYPE } from './buttonBar.constants';

export const catalogExportCasetype = id => ({
  type: CATALOG_EXPORT_CASETYPE,
  payload: { id },
});
