import { combineReducers } from 'redux';
import items from './Items/items.reducer';
import details from './Details/details.reducer';

export const catalog = combineReducers({
  items,
  details,
});

export default catalog;
