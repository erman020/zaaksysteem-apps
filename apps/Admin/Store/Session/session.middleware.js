import { get } from '@mintlab/kitchen-sink';
import { login } from './session.actions';
import { LOGIN_PATH, LOGOUT_PATH } from '../../../library/auth';
import { getUrl, navigate } from '../../../library/url';
import {
  SESSION_AUTH_LOGIN,
  SESSION_AUTH_LOGOUT,
  SESSION_FETCH,
} from './session.constants';

const handleSessionFetchSuccess = (store, next, action) => {
  next(action);
  if (!get(store.getState(), 'session.data.logged_in_user')) {
    store.dispatch(login(getUrl()));
  }
};

const handleLogin = (store, next, action) => {
  next(action);
  navigate(`${LOGIN_PATH}?referer=${action.payload}`);
};

const handleLogout = (store, next, action) => {
  next(action);
  navigate(LOGOUT_PATH);
};

export const sessionMiddleware = store => next => action => {
  switch (action.type) {
    case SESSION_FETCH.SUCCESS:
      return handleSessionFetchSuccess(store, next, action);
    case SESSION_AUTH_LOGIN:
      return handleLogin(store, next, action);
    case SESSION_AUTH_LOGOUT:
      return handleLogout(store, next, action);
    default:
      return next(action);
  }
};

export default sessionMiddleware;
