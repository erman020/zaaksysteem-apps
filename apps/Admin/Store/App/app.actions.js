import {
  APP_BOOTSTRAP_INIT,
  APP_BOOTSTRAP_VALID,
  APP_BOOTSTRAP_ERROR,
} from './app.constants';

export const init = payload => ({
  type: APP_BOOTSTRAP_INIT,
  payload,
});

export const valid = payload => ({
  type: APP_BOOTSTRAP_VALID,
  payload,
});

export const error = payload => ({
  type: APP_BOOTSTRAP_ERROR,
  payload,
});
