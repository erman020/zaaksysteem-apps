import { get } from '@mintlab/kitchen-sink';
import { showSnackbar } from '../UI/ui.actions';
import { login } from '../Session/session.actions';

export const errorMiddleware = store => next => action => {
  const { dispatch } = store;

  const ajax = get(action, 'ajax');
  const error = get(action, 'error');
  const errorType = get(action, 'payload.error');

  const statusHandlers = {
    401() {
      return login();
    },
    403() {
      return showSnackbar('server:status403');
    },
    500() {
      return showSnackbar('server:status500');
    },
  };

  if (ajax && error) {
    if (statusHandlers.hasOwnProperty(errorType)) {
      dispatch(statusHandlers[errorType]());
    }
  }

  return next(action);
};

export default errorMiddleware;
