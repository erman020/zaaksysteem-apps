import { APP_BOOTSTRAP_VALID, APP_BOOTSTRAP_ERROR } from './app.constants';

const initialState = {
  bootstrap: 'pending',
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function app(state = initialState, action) {
  switch (action.type) {
    case APP_BOOTSTRAP_VALID:
      return {
        ...state,
        bootstrap: 'valid',
      };
    case APP_BOOTSTRAP_ERROR:
      return {
        ...state,
        bootstrap: 'error',
      };
    default:
      return state;
  }
}
