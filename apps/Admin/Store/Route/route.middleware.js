import { ROUTE_INVOKE } from './route.constants';
import { resolve } from './route.actions';
import { pushState, replaceState } from '../../../library/dom/history';

const handleInvoke = (store, next, action) => {
  next(action);

  const { replace, path } = action.payload;

  const updateHistory = replace ? replaceState : pushState;
  updateHistory(path);
  store.dispatch(resolve(action.payload));
};

export const routeMiddleware = store => next => action => {
  switch (action.type) {
    case ROUTE_INVOKE:
      return handleInvoke(store, next, action);
    default:
      return next(action);
  }
};

export default routeMiddleware;
