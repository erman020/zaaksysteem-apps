import { get } from '@mintlab/kitchen-sink';
import { fetchSystemConfiguration } from './systemconfiguration.actions';
import { showDialog, hideDialog, showSnackbar } from '../UI/ui.actions';
import { invoke } from '../Route/route.actions';
import { ROUTE_RESOLVE } from '../Route/route.constants';
import {
  SYSTEMCONFIGURATION_SAVE,
  SYSTEMCONFIGURATION_DISCARD,
} from './systemconfiguration.constants';
import {
  shouldFetch,
  shouldDispatch,
} from './library/systemConfiguration.functions';

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 */
export const handleRouteInvoke = (store, next, action) => {
  const { path, force } = action.payload;
  const state = store.getState();

  // Dispatch systemconfig
  if (shouldFetch(state, path)) {
    store.dispatch(fetchSystemConfiguration());
  }

  // Dispatch dialog
  if (shouldDispatch(state, path, force)) {
    store.dispatch(
      showDialog({
        type: 'SystemConfigDiscardChanges',
        options: action.payload,
      })
    );
  } else {
    next(action);
  }
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const handleSaveSuccess = (store, next, action) => {
  store.dispatch(showSnackbar('config:saved'));

  return next(action);
};

/**
 * @param {Object} store
 * @param {Function} next
 * @param {Object} action
 * @returns {Object}
 */
export const handleDiscard = (store, next, action) => {
  store.dispatch(
    invoke({
      path: get(action, 'payload.path'),
      force: true,
    })
  );

  store.dispatch(hideDialog());

  return next(action);
};

/**
 * @param {Object} store
 * @returns {Function}
 */
export const systemConfigurationMiddleware = store => next => action => {
  switch (action.type) {
    case ROUTE_RESOLVE:
      return handleRouteInvoke(store, next, action);
    case SYSTEMCONFIGURATION_SAVE.SUCCESS:
      return handleSaveSuccess(store, next, action);
    case SYSTEMCONFIGURATION_DISCARD:
      return handleDiscard(store, next, action);
    default:
      return next(action);
  }
};

export default systemConfigurationMiddleware;
