import thunkMiddleware from 'redux-thunk';
import catalogMiddleware from './Catalog/catalog.middleware';
import logMiddleware from './Log/log.middleware';
import systemConfigurationMiddleware from './SystemConfiguration/systemconfiguration.middleware';
import sessionMiddleware from './Session/session.middleware';
import errorMiddleware from './App/error.middleware';
import bootstrapMiddleware from './App/bootstrap.middleware';
import routeMiddleware from './Route/route.middleware';

/** NOTE: the order of the reducers is important here.
 *
 * - Thunkmiddleware needs to be first.
 *
 * - routeMiddleware needs to be last. Other middleware
 * needs to be able to cancel the route change.
 *
 */
export default [
  thunkMiddleware,
  bootstrapMiddleware,
  errorMiddleware,
  systemConfigurationMiddleware,
  catalogMiddleware,
  logMiddleware,
  sessionMiddleware,
  routeMiddleware,
];
