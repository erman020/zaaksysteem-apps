import { cloneWithout } from '@mintlab/kitchen-sink';
import {
  UI_BANNER_SHOW,
  UI_BANNER_HIDE,
  UI_DRAWER_OPEN,
  UI_DRAWER_CLOSE,
  UI_DIALOG_SHOW,
  UI_DIALOG_HIDE,
  UI_SNACKBAR_SHOW,
  UI_SNACKBAR_HIDE,
  UI_OVERLAY_OPEN,
  UI_OVERLAY_CLOSE,
  UI_WINDOW_LOAD,
  UI_WINDOW_UNLOAD,
} from './ui.constants';

const initialState = {
  banners: {},
  drawer: false,
  iframe: {
    overlay: false,
    loading: true,
  },
  snackbar: null,
  dialog: null,
};

/* eslint-disable complexity */
/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function ui(state = initialState, action) {
  const { payload, type } = action;

  switch (type) {
    case UI_BANNER_SHOW:
      return {
        ...state,
        banners: {
          ...state.banners,
          [payload.identifier]: {
            ...payload,
          },
        },
      };
    case UI_BANNER_HIDE:
      return {
        ...state,
        banners: cloneWithout(state.banners, payload.identifier),
      };
    case UI_DRAWER_OPEN:
      return {
        ...state,
        drawer: true,
      };
    case UI_DRAWER_CLOSE:
      return {
        ...state,
        drawer: false,
      };
    case UI_DIALOG_SHOW:
      return {
        ...state,
        dialog: {
          ...payload,
        },
      };
    case UI_DIALOG_HIDE:
      return {
        ...state,
        dialog: null,
      };
    case UI_SNACKBAR_SHOW:
      return {
        ...state,
        snackbar: {
          message: payload,
        },
      };
    case UI_SNACKBAR_HIDE:
      return {
        ...state,
        snackbar: null,
      };
    case UI_OVERLAY_OPEN:
      return {
        ...state,
        iframe: {
          ...state.iframe,
          overlay: true,
        },
      };
    case UI_OVERLAY_CLOSE:
      return {
        ...state,
        iframe: {
          ...state.iframe,
          overlay: false,
        },
      };
    case UI_WINDOW_LOAD:
      return {
        ...state,
        iframe: {
          ...state.iframe,
          loading: false,
        },
      };
    case UI_WINDOW_UNLOAD:
      return {
        ...state,
        iframe: {
          ...state.iframe,
          loading: true,
        },
      };
    default:
      return state;
  }
}
