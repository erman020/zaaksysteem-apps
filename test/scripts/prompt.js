const inquirer = require('inquirer');
const { exec } = require('child_process');
const { readFile } = require('fs');

const environments = [
  { name: 'local', url: 'https://dev.zaaksysteem.nl' },
  { name: 'development', url: 'https://development.zaaksysteem.nl' },
  { name: 'hotfix', url: 'https://hotfix.zaaksysteem.nl' },
];

const getCredentialsFromDisk = () =>
  new Promise(resolve => {
    readFile('credentials.json', 'utf8', function(err, data) {
      if (err) {
        resolve({});
      }

      resolve(JSON.parse(data));
    });
  });

const promptEnviroment = async () => {
  const { environment } = await inquirer.prompt({
    type: 'list',
    name: 'environment',
    message: 'Which environment would you like to test?',
    choices: environments.map(value => value.name),
  });

  return environments.find(item => item.name === environment);
};

const promptCredentials = async () =>
  inquirer.prompt([
    {
      type: 'input',
      name: 'username',
      message: 'Username',
      default: 'admin',
    },
    {
      type: 'input',
      name: 'password',
      message: 'Password',
      default: 'admin',
    },
  ]);

const getUserInput = async () => {
  const credentials = await getCredentialsFromDisk();
  const environment = await promptEnviroment();
  const environmentCredentials = credentials[environment.name];
  const { username, password } = environmentCredentials
    ? environmentCredentials
    : await promptCredentials();

  return {
    url: environment.url,
    username,
    password,
  };
};

const startCypress = ({ username, password, url }) =>
  exec(
    `CYPRESS_baseUrl=${url} CYPRESS_username=${username} CYPRESS_password=${password} npm run cypress:interactive`
  );

const start = async () => {
  const options = await getUserInput();
  startCypress(options);
};

start();
