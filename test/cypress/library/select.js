export const selectAutoCompleteValue = (selector, value) => {
  cy.get(`${selector} .react-select__control`).click();
  cy.get(`${selector} .react-select__input input`).type(value, {
    force: true,
  });

  const getItems = () => cy.get(`${selector} .react-select__option`);

  return {
    items: getItems,
    clickOnFirstResult: () => {
      return getItems().then(([item]) => item.click());
    },
  };
};
