import {
  DRAWER_MENU_TOGGLE,
  USER_MENU_TOGGLE,
  USER_MENU_LOGOUT,
} from '../constants/app';
/**
 * Get element by data-scope value
 */
Cypress.Commands.add('getScoped', item => cy.get(`[data-scope="${item}"]`));

/**
 * Login to Zaaksysteem admin
 */
Cypress.Commands.add('login', () => {
  cy.clearCookies();
  cy.visit('/admin');
  cy.get('#id_username').type(Cypress.env('username'));
  cy.get('#id_password').type(Cypress.env('password'));
  cy.get('input[type="submit"]').click();
});

/**
 * Logout of Zaaksysteem admin
 */
Cypress.Commands.add('logout', () => {
  cy.getScoped(USER_MENU_TOGGLE).click();
  cy.getScoped(USER_MENU_LOGOUT).click();
});

/**
 * Navigate using drawer
 */
Cypress.Commands.add('drawerNavigate', item => {
  cy.getScoped(DRAWER_MENU_TOGGLE).click();
  cy.getScoped(item).click();
});
