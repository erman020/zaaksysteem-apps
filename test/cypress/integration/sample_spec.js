import { DRAWER_LINK_LOG, DRAWER_LINK_CONFIGURATIE } from '../constants/app';

describe('Sample spec', () => {
  it('successfully loads', () => {
    cy.visit('/admin');
  });

  it('successfully logs in', () => {
    cy.login();
    cy.url().should('include', '/admin');
  });

  it('can navigate to log', () => {
    cy.drawerNavigate(DRAWER_LINK_LOG);
    cy.url().should('include', '/admin/logboek');
  });

  it('can navigate to configuration', () => {
    cy.drawerNavigate(DRAWER_LINK_CONFIGURATIE);
    cy.url().should('include', '/admin/configuratie');
  });

  it('can successfully logout', () => {
    cy.logout();
  });
});
