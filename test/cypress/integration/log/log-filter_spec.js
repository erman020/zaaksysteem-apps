import { DRAWER_LINK_LOG } from '../../constants/app';
import { selectAutoCompleteValue } from '../../library/select';

const FILTER_OPEN = 'toggle-filter:search:button';
const FILTER_CLOSE = 'toggle-filter:close:button';
const FILTER_KEYWORD_INPUT = 'log-filter:keyword:text-field-input';
const FILTER_CASENUMBER_INPUT = 'log-filter:casenumber:text-field-input';
const LOG_LOADER = 'log-loader:loader';

const openFilters = () => cy.getScoped(FILTER_OPEN).click();
const closeFilters = () => cy.getScoped(FILTER_CLOSE).click();
const applyFilter = (filter, value, submit = false) => {
  const filterItem = cy.getScoped(filter);
  filterItem.type(value);

  if (submit) {
    filterItem.type('{enter}');
  }
};
const shouldReloadResults = () => {
  cy.getScoped(LOG_LOADER).should('be.visible');
  cy.getScoped(LOG_LOADER).should('not.be.visible');
};

describe('Log filters', () => {
  before(() => cy.login());
  after(() => cy.logout());

  beforeEach(() => Cypress.Cookies.preserveOnce('zaaksysteem_session'));

  it('successfully loads', () => {
    cy.drawerNavigate(DRAWER_LINK_LOG);
    cy.getScoped(LOG_LOADER).should('not.be.visible');
  });

  it('can toggle filter visibility', () => {
    openFilters();

    cy.getScoped(FILTER_KEYWORD_INPUT).should('be.visible');

    closeFilters();

    cy.getScoped(FILTER_KEYWORD_INPUT).should('not.be.visible');
    shouldReloadResults();
  });

  it('can filter on keyword', () => {
    openFilters();
    applyFilter(FILTER_KEYWORD_INPUT, 'Zaak', true);

    cy.url().should('include', 'keyword');
    shouldReloadResults();

    closeFilters();
  });

  it('can filter on caseNumber', () => {
    openFilters();
    applyFilter(FILTER_CASENUMBER_INPUT, '1', true);

    cy.url().should('include', 'caseNumber');
    shouldReloadResults();

    closeFilters();
  });

  it('can filter on user', () => {
    openFilters();
    selectAutoCompleteValue('div', 'admi').clickOnFirstResult();

    cy.url().should('include', 'user=');
    shouldReloadResults();

    closeFilters();
  });

  it('can set filters from url', () => {
    const keyword = 'Zaak';
    const caseNumber = '1';

    openFilters();
    applyFilter(FILTER_KEYWORD_INPUT, keyword);
    applyFilter(FILTER_CASENUMBER_INPUT, caseNumber, true);

    cy.reload();

    cy.getScoped(FILTER_KEYWORD_INPUT)
      .should('be.visible')
      .should('have.value', keyword);

    cy.getScoped(FILTER_CASENUMBER_INPUT)
      .should('be.visible')
      .should('have.value', caseNumber);
  });

  it('resets the page to 1 after applying filers', () => {
    cy.visit('/admin/logboek/3/50');
    openFilters();
    applyFilter(FILTER_KEYWORD_INPUT, 'Zaak', true);

    cy.url().should('include', 'logboek/1/50');
  });
});
