#!/bin/bash

# Copy the certificates from the named `certificates:` volume
# that must be mounted in the zaaksysteem `apps` service,
# so that `webpack-serve` running as the built-in `node` user
# can read them.
cp -r /etc/nginx/ssl /opt/zaaksysteem-apps/ssl
chown -R node:node /opt/zaaksysteem-apps/ssl

nginx -g 'daemon off;'
