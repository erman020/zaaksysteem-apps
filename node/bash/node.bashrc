WHALE="🐳  "

if [ $PS1 != $WHALE ]; then
  export PS1=$WHALE
  alias ls="pwd && ls -al"

  ENTRY_POINT=$PWD
  NODE_VERSION=$(node --version | sed -r 's/^v//')
  NPM=$(which npm)
  NPM_VERSION=$(npm --version)
  NPX_VERSION=$(npx --version)
  USER=$(whoami)
  WORKSPACE_MATCH="^${ENTRY_POINT}(/|$)$"
  YARN=$(which yarn)
  YARN_VERSION=$(yarn --version)

  alias zs="${ENTRY_POINT}/bin/zs"

  color() {
    echo "\e[33m${1}\e[0m"
  }

  message() {
    echo -e "Please use the $(color zs) command."
    echo ""
    echo -e "If you have a good reason, you can run $(color ${1}) by"
    echo -e " - using the absolute path: $(color "\$(which ${1}) [command] [flags]")"
    echo -e " - leaving the $(color ${ENTRY_POINT}) directory"
    echo ""
    echo -e "NEVER manage $(color zaaksysteem-apps) dependencies directly with $(color ${1})!"
  }

  npm() {
    if [[ $PWD =~ $WORKSPACE_MATCH ]]; then
      message npm
    else
      $NPM $@
    fi
  }

  yarn() {
    if [[ $PWD =~ $WORKSPACE_MATCH ]]; then
      message yarn
    else
      $YARN $@
    fi
  }

  echo "Initialized Bash for the development container."
  echo -e "$(color node): ${NODE_VERSION}"
  echo -e " $(color npm): ${NPM_VERSION}"
  echo -e " $(color npx): ${NPX_VERSION}"
  echo -e "$(color yarn): ${YARN_VERSION}"
  echo -e "You are user $(color ${USER}) working in $(color ${ENTRY_POINT})."
  echo -e "The $(color zs) command is your friend."
fi
