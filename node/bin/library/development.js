const { IS_STANDALONE } = require('./constants');

/* Constants that are only used in development.
 * See also package.json#devDependencies
 */
const internalIp = require('internal-ip');

const { assign } = Object;

const DEVELOPMENT_HOST = IS_STANDALONE ? 'localhost' : 'dev.zaaksysteem.nl';
const DEVELOPMENT_PORT = 8081;

assign(exports, {
  DEVELOPMENT_HOST,
  DEVELOPMENT_PORT,

  /**
   * Webpack public path used in the `output`
   * configuration and the SPA entry HTML template.
   *
   * @type {string}
   */
  PUBLIC_PATH: `//${DEVELOPMENT_HOST}:${DEVELOPMENT_PORT}/`,
  WEBSOCKET_PORT: 8082,
  INTERNAL_IP: internalIp.v4.sync(),
});
