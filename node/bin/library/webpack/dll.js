/********************************************
 * Helpers for using the webpack DLL plugins,
 * connecting the vendor and app bundles.
 */

const { join } = require('path');
const {
  DllPlugin,
  DllReferencePlugin,
} = require('webpack');
const {
  NODE_ROOT,
  VENDOR_SERVER_PATH,
} = require('../constants');

/**
 * This must be expanded to a valid ECMAScript identifier,
 * relating the webpack configuration `output.library`
 * property and the DllPlugin `name` property.
 *
 * @type {string}
 */
const library = '[name]_[hash]';

/**
 * Webpack output file name pattern.
 *
 * @type {string}
 */
const manifestFile = '[name]-manifest.json';

const getManifestPath = name =>
  join(VENDOR_SERVER_PATH, `${name}-manifest.json`);

const getManifest = name =>
  require(getManifestPath(name));

/**
 * @return {DllPlugin}
 */
const dllPluginFactory = () =>
  new DllPlugin({
    name: library,
    path: join(VENDOR_SERVER_PATH, manifestFile),
  });

/**
 * @param {string} name
 *   The webpack `[name]` expansion.
 * @return {DllReferencePlugin}
 */
const dllReferencePluginFactory = name =>
  new DllReferencePlugin({
    context: NODE_ROOT,
    manifest: getManifest(name),
  });

module.exports = {
  dllPluginFactory,
  dllReferencePluginFactory,
  library,
};
