// Webpack loaders

/**
 * @see https://cssdb.org/#staging-process
 *
 * @type {number}
 */
const POSTCSS_PRESET_ENV_STAGE = 3;

/**
 * Getters dictionary for webpack loader options.
 * Every method is a factory for the respective loader.
 *
 * @type {Object}
 */
const typeOptions = {
  /**
   * Get `css-loader` options (overloaded).
   *
   * @private
   * @param {string} [localIdentName]
   * @return {Object}
   */
  css(localIdentName) {
    if (typeof localIdentName === 'string') {
      return {
        importLoaders: 1,
        localIdentName,
        modules: true,
      };
    }

    return {
      modules: false,
    };
  },
  /**
   * Get `babel-loader` options (none).
   *
   * Babel options MUST be set in `.babelrc.js`
   * for interoperabilty with other tools.
   *
   * @private
   * @return {Object}
   */
  babel() {
    return {};
  },
  /**
   * Get `file-loader` options (none).
   *
   * @private
   * @return {Object}
   */
  file() {
    return {};
  },
  /**
   * Get `postcss-loader` options (fixture).
   *
   * @private
   * @return {Object}
   */
  postcss() {
    const postcssPresetEnv = require('postcss-preset-env');

    return {
      ident: 'postcss',
      plugins: () => [
        postcssPresetEnv({
          stage: POSTCSS_PRESET_ENV_STAGE,
        }),
      ],
    };
  },
  /**
   * Get `style-loader` options (none).
   *
   * @private
   * @return {Object}
   */
  style() {
    return {};
  },
};

/**
 * Get a webpack loader configuration object.
 *
 * @param {string} type
 * @param {Object} options
 * @return {Object}
 */
const loader = (type, options) => ({
  loader: `${type}-loader`,
  options: typeOptions[type](options),
});

module.exports = loader;
