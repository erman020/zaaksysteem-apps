/*
 * Production only: extract CSS to a file.
 */
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const loader = require('./loader');
const {
  packageRule,
  rule,
} = require('./rule');

const extractText = use =>
  ExtractTextPlugin
    .extract({
      fallback: 'style-loader',
      use,
    });

/**
 * @param {string} app
 *   The App name as found in its manifest.
 * @param {Object} configuration
 *   Webpack configuration object.
 */
function build(app, {
  module: {
    rules,
  },
  output,
  plugins,
}) {
  const styleRules = [
    rule('css', extractText([
      loader('css', '[hash:base64:8]'),
      loader('postcss'),
    ])),
    packageRule('css', extractText([
      loader('css'),
    ])),
  ];

  output.publicPath = `/${app}/`;
  rules.push(...styleRules);
  plugins.push(
    // https://github.com/webpack-contrib/extract-text-webpack-plugin/issues/763
    new ExtractTextPlugin(`${app}-[md5:contenthash:hex:20].css`)
  );
}

module.exports = build;
