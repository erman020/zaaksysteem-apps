const { join } = require('path');
const { NamedModulesPlugin } = require('webpack');
const loader = require('./loader');
const { packageRule, rule } = require('./rule');
const { PUBLIC_PATH } = require('../development');
const { NODE_ROOT } = require('../constants');

const styleSheetRules = [
  rule('css', [
    loader('style'),
    loader('css', '[local]___[hash:base64:5]'),
    loader('postcss'),
  ]),
  packageRule('css', [
    loader('style'),
    loader('css'),
  ]),
];

/**
 * @param {Object} configuration
 */
function serve(configuration) {
  const {
    module: {
      rules,
    },
    output,
    plugins,
    resolve: {
      modules,
    },
  } = configuration;

  configuration.devtool = 'cheap-module-eval-source-map';
  rules.push(...styleSheetRules);
  output.publicPath = PUBLIC_PATH;
  plugins.push(new NamedModulesPlugin());
  // webpack hmr resolution
  modules.push(join(NODE_ROOT, 'node_modules'));
}

module.exports = serve;
