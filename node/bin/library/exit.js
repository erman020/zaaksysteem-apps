const { error } = require('./methods');

const EXIT_CODE_ERROR = 1;

/**
 * @param {string} message
 */
function exit(message) {
  error(`${message}, aborting`);
  process.exit(EXIT_CODE_ERROR);
}

module.exports = exit;
