const { sync: deleteSynchronously } = require('del');

/**
 * Remove all children, excluding the parent.
 *
 * @see https://www.npmjs.com/package/del#beware
 *
 * @param {string} absolutePath
 */
function removeChildren(absolutePath) {
  deleteSynchronously([
    `${absolutePath}/**`,
    `!${absolutePath}`,
  ], {
    force: true,
  });
}

/**
 * Remove the webpack DLL plugin manifests from the document root.
 */
function removeManifests() {
  // ZS-TODO: Using the constants can create circular dependencies.
  const {
    env: {
      SERVER_ROOT,
    },
  } = process;

  deleteSynchronously([
    `${SERVER_ROOT}/vendor/*-manifest.json`,
  ], {
    force: true,
  });
}

module.exports = {
  removeChildren,
  removeManifests,
};
