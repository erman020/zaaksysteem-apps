###
### Welcome to the Zaaksysteem Apps Dockerfile
###
### Directory layout within docker:
### /opt/zaaksysteem-apps:
###   /dev-bin:               Shell scripts
###   /node:                  Working directory
###     /bin:                 Package script executables
###     /node_modules:        Build dependencies
###     /public:              Subordinary artefacts (view at http://localhost:8080)
###     /src:                 App sources
###     /vendor:              Vendor code
###       /node_modules:      Client dependencies
###   /npm:                   package*.json (VCS)
###   /root:                  Compiled "build artifacts"
###     /admin:               Example: admin app
###     /vendor:              Pre-built client dependencies
###   /ssl:                   Copied from another mount
###
### Current base layer:       Debian Stretch
###
### We build node 8 on Debian Stretch, and are building nginx manually on this layer
### in another stage. This way we know for sure there are no difference in node and npm
### in the development tools.
###
### Philosophy (in progress):
###     Stage: layer-setup    sets up the npm and node layer
###     Stage: nginx          sets up nginx for development purposes
###     Stage: apps-src       builds the sources, starting with shared
###     Stage: production     production build, no webpack, no
###
### HEADS UP: The `USER` command only affects `RUN`, `CMD` and `ENTRYPOINT`.
### In particular, be careful using
### - `WORKDIR` with a directory that doesn't exist yet
### - `COPY --chown` of files to a directory that doesn't exist yet
### Rule of thumb: always create the directories first.

ARG CONTAINER_ROOT=/opt/zaaksysteem-apps
ARG NODE_ROOT=$CONTAINER_ROOT/node
ARG SOURCE_ROOT=$NODE_ROOT/src
ARG VENDOR_ROOT=$NODE_ROOT/vendor

ARG NPM_ROOT=$CONTAINER_ROOT/npm
ARG NPM_BUILD_ROOT=$NPM_ROOT/build
ARG NPM_VENDOR_ROOT=$NPM_ROOT/vendor

ARG SERVER_ROOT=$CONTAINER_ROOT/root


### --------- Stage: Setup layer ---------
### Shared libraries
FROM node:8-stretch AS layer-setup

ARG CONTAINER_ROOT
ARG SERVER_ROOT

### Update npm (>= 6.1.0 provides `audit`) and layout the directories
RUN npm install -g npm@6.1.0\
  && mkdir -p $SERVER_ROOT\
  && chown -R node:node $CONTAINER_ROOT


### --------- Stage: Setup NGINX for development ---------
### (As early as possible, takes some time, but prevent it
### from installing when target "production" is set)
FROM layer-setup AS nginx

RUN set -x\
  && apt-get update\
  && apt-get install --no-install-recommends --no-install-suggests -y nginx
WORKDIR /etc/nginx
COPY Docker/nginx/conf/nginx.conf /etc/nginx/nginx.conf


### --------- Stage: Install Node.js build dependencies ---------
FROM layer-setup AS install-build-dependencies

ARG NODE_ROOT
ARG NPM_BUILD_ROOT

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

COPY --chown=node:node ./node  $NODE_ROOT
COPY --chown=node:node ./npm/build  $NPM_BUILD_ROOT

USER node
WORKDIR $NODE_ROOT

RUN ln -s ../npm/build/package.json package.json\
  && ln -s ../npm/build/package-lock.json package-lock.json\
  && npm install --no-optional --loglevel error


### --------- Stage: Install client-side dependencies and build browser bundles ---------
FROM install-build-dependencies AS build-vendor

ARG CONTAINER_ROOT
ENV CONTAINER_ROOT $CONTAINER_ROOT

ARG SERVER_ROOT
ENV SERVER_ROOT $SERVER_ROOT

ARG SOURCE_ROOT
ENV SOURCE_ROOT $SOURCE_ROOT

ARG VENDOR_ROOT
ENV VENDOR_ROOT $VENDOR_ROOT

ARG NPM_VENDOR_ROOT

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

COPY --chown=node:node ./npm/vendor $NPM_VENDOR_ROOT

RUN mkdir ./vendor\
  && ln -s ../../npm/vendor/package.json ./vendor/package.json\
  && ln -s ../../npm/vendor/package-lock.json ./vendor/package-lock.json\
  && npm install --prefix ./vendor --no-optional --loglevel error\
  && ./bin/vendor


### --------- Stage: Build all apps ---------
### Despite the webapck DLL manifests, the node_modules from
### `build-vendor` must be resolvable during the webpack build.
FROM build-vendor AS build-apps

ARG CONTAINER_ROOT
ENV CONTAINER_ROOT $CONTAINER_ROOT

ARG NODE_ROOT
ENV NODE_ROOT $NODE_ROOT

ARG SERVER_ROOT
ENV SERVER_ROOT $SERVER_ROOT

ARG SOURCE_ROOT
ENV SOURCE_ROOT $SOURCE_ROOT

ARG WEBPACK_BUILD_TARGET
ENV WEBPACK_BUILD_TARGET $WEBPACK_BUILD_TARGET

ARG COMMIT_SHA
ENV VERSION_INFO $COMMIT_SHA

# NODE_ENV is always production

COPY --chown=node:node ./apps $SOURCE_ROOT

RUN echo "BUILD TARGET: $WEBPACK_BUILD_TARGET" \
  && ./bin/build \
  && cp ./src/Admin/legacy.css ../root/admin/ \
  && echo "Git commit: $VERSION_INFO" > ../root/admin/version.txt


### --------- Stage: production ---------
FROM nginx AS production

ARG SERVER_ROOT

USER root
COPY --from=build-apps $SERVER_ROOT $SERVER_ROOT
CMD ["nginx", "-g", "daemon off;"]


### --------- Stage: development ---------
FROM nginx AS development

ARG CONTAINER_ROOT
ENV CONTAINER_ROOT $CONTAINER_ROOT

ARG NODE_ROOT
ENV NODE_ROOT $NODE_ROOT

ARG SERVER_ROOT
ENV SERVER_ROOT $SERVER_ROOT

ARG SOURCE_ROOT
ENV SOURCE_ROOT $SOURCE_ROOT

ARG VENDOR_ROOT
ENV VENDOR_ROOT $VENDOR_ROOT

ENV NODE_ENV development

COPY --chown=node:node --from=build-apps $SERVER_ROOT $SERVER_ROOT
COPY --chown=node:node --from=build-apps $NODE_ROOT $NODE_ROOT
COPY ./dev-bin/develop.sh $CONTAINER_ROOT/dev-bin/
COPY --chown=node:node ./node/bash $NODE_ROOT/bash
COPY --chown=node:node .prettierrc $NODE_ROOT/.prettierrc

# devDependencies are not installed yet
RUN npm install --no-optional --loglevel error

WORKDIR $NODE_ROOT
ENTRYPOINT ${CONTAINER_ROOT}/dev-bin/develop.sh
